<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PropertyCompletionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PropertyCompletionsTable Test Case
 */
class PropertyCompletionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PropertyCompletionsTable
     */
    public $PropertyCompletions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.property_completions',
        'app.properties',
        'app.projects',
        'app.developers',
        'app.milestones',
        'app.completion_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PropertyCompletions') ? [] : ['className' => PropertyCompletionsTable::class];
        $this->PropertyCompletions = TableRegistry::get('PropertyCompletions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PropertyCompletions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
