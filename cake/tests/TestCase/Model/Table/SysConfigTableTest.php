<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SysConfigTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SysConfigTable Test Case
 */
class SysConfigTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SysConfigTable
     */
    public $SysConfig;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sys_config'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SysConfig') ? [] : ['className' => SysConfigTable::class];
        $this->SysConfig = TableRegistry::get('SysConfig', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SysConfig);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
