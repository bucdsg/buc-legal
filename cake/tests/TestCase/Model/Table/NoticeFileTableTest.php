<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NoticeFileTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NoticeFileTable Test Case
 */
class NoticeFileTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NoticeFileTable
     */
    public $NoticeFile;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.notice_file',
        'app.notices',
        'app.properties',
        'app.projects',
        'app.developers',
        'app.milestones',
        'app.property_completions',
        'app.property_completion_types',
        'app.clients',
        'app.lawyers',
        'app.recipients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NoticeFile') ? [] : ['className' => NoticeFileTable::class];
        $this->NoticeFile = TableRegistry::get('NoticeFile', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NoticeFile);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
