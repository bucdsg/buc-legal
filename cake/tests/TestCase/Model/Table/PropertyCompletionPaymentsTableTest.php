<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PropertyCompletionPaymentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PropertyCompletionPaymentsTable Test Case
 */
class PropertyCompletionPaymentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PropertyCompletionPaymentsTable
     */
    public $PropertyCompletionPayments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.property_completion_payments',
        'app.property_completions',
        'app.properties',
        'app.projects',
        'app.developers',
        'app.clients',
        'app.lawyers',
        'app.milestones',
        'app.property_completion_types',
        'app.notices',
        'app.property',
        'app.stage_completion',
        'app.completion_payments',
        'app.files'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PropertyCompletionPayments') ? [] : ['className' => PropertyCompletionPaymentsTable::class];
        $this->PropertyCompletionPayments = TableRegistry::get('PropertyCompletionPayments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PropertyCompletionPayments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
