<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PropertyCompletionTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PropertyCompletionTypesTable Test Case
 */
class PropertyCompletionTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PropertyCompletionTypesTable
     */
    public $PropertyCompletionTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.property_completion_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PropertyCompletionTypes') ? [] : ['className' => PropertyCompletionTypesTable::class];
        $this->PropertyCompletionTypes = TableRegistry::get('PropertyCompletionTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PropertyCompletionTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
