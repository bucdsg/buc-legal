<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PropertyCompletionPayments Model
 *
 * @property \App\Model\Table\PropertyCompletionsTable|\Cake\ORM\Association\BelongsTo $PropertyCompletions
 *
 * @method \App\Model\Entity\PropertyCompletionPayment get($primaryKey, $options = [])
 * @method \App\Model\Entity\PropertyCompletionPayment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PropertyCompletionPayment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PropertyCompletionPayment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PropertyCompletionPayment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PropertyCompletionPayment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PropertyCompletionPayment findOrCreate($search, callable $callback = null, $options = [])
 */
class PropertyCompletionPaymentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('property_completion_payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('PropertyCompletions', [
            'foreignKey' => 'property_completion_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('mode_of_payment');

        $validator
            ->integer('amount')
            ->allowEmpty('amount');

        $validator
            ->allowEmpty('status');

        $validator
            ->dateTime('paid_at')
            ->allowEmpty('paid_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['property_completion_id'], 'PropertyCompletions'));

        return $rules;
    }
}
