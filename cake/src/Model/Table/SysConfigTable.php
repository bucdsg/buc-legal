<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SysConfig Model
 *
 * @method \App\Model\Entity\SysConfig get($primaryKey, $options = [])
 * @method \App\Model\Entity\SysConfig newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SysConfig[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SysConfig|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SysConfig patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SysConfig[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SysConfig findOrCreate($search, callable $callback = null, $options = [])
 */
class SysConfigTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sys_config');
        $this->primaryKey('config_key');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        // $validator->allowEmpty('config_key');

        // $validator->allowEmpty('config_value');

        return $validator;
    }
}
