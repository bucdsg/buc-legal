<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PropertyCompletions Model
 *
 * @property \App\Model\Table\PropertiesTable|\Cake\ORM\Association\BelongsTo $Properties
 * @property \App\Model\Table\CompletionTypesTable|\Cake\ORM\Association\BelongsTo $CompletionTypes
 *
 * @method \App\Model\Entity\PropertyCompletion get($primaryKey, $options = [])
 * @method \App\Model\Entity\PropertyCompletion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PropertyCompletion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PropertyCompletion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PropertyCompletion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PropertyCompletion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PropertyCompletion findOrCreate($search, callable $callback = null, $options = [])
 */
class PropertyCompletionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('property_completions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Properties', [
            'foreignKey' => 'property_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PropertyCompletionTypes', [
            'foreignKey' => 'completion_type_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Notices', [
            'className' => 'Notices'
        ]);
        $this->hasMany('CompletionPayments', [
            'className' => 'PropertyCompletionPayments'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('notes');

        $validator
            ->dateTime('scheduled_at')
            ->allowEmpty('scheduled_at');

        $validator
            ->dateTime('completed_at')
            ->allowEmpty('completed_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['property_id'], 'Properties'));
        // $rules->add($rules->existsIn(['completion_type_id'], 'PropertyCompletionTypes'));

        return $rules;
    }
}
