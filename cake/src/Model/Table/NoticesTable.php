<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Notices Model
 *
 * @property \App\Model\Table\PropertiesTable|\Cake\ORM\Association\BelongsTo $Properties
 * @property \App\Model\Table\LawyersTable|\Cake\ORM\Association\BelongsTo $Lawyers
 * @property \App\Model\Table\PropertyCompletionsTable|\Cake\ORM\Association\BelongsTo $PropertyCompletions
 * @property \App\Model\Table\RecipientsTable|\Cake\ORM\Association\BelongsTo $Recipients
 * @property \App\Model\Table\NoticeFileTable|\Cake\ORM\Association\HasMany $NoticeFile
 *
 * @method \App\Model\Entity\Notice get($primaryKey, $options = [])
 * @method \App\Model\Entity\Notice newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Notice[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Notice|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Notice patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Notice[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Notice findOrCreate($search, callable $callback = null, $options = [])
 */
class NoticesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('notices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Property', [
            'foreignKey' => 'property_id',
            'className' => 'Properties'
        ]);
        $this->belongsTo('Lawyers', [
            'foreignKey' => 'lawyer_id'
        ]);
        $this->belongsTo('StageCompletion', [
            'foreignKey' => 'property_completion_id',
            'className' => 'PropertyCompletions'
        ]);
        $this->hasMany('Files', [
            'foreignKey' => 'notice_id',
            'className' => 'NoticeFile'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('notice_type');

        $validator
            ->allowEmpty('recipient_entity');

        $validator
            ->allowEmpty('recipient_type');

        $validator
            ->allowEmpty('recipient_email');

        $validator
            ->allowEmpty('recipient_phone');

        $validator
            ->dateTime('last_sent')
            ->allowEmpty('last_sent');

        $validator
            ->integer('created_at')
            ->allowEmpty('created_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['property_id'], 'Properties'));
        // $rules->add($rules->existsIn(['lawyer_id'], 'Lawyers'));
        // $rules->add($rules->existsIn(['property_completion_id'], 'PropertyCompletions'));

        return $rules;
    }
}
