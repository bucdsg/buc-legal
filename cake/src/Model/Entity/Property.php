<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Property Entity
 *
 * @property int $id
 * @property int $project_id
 * @property int $client_id
 * @property string $name
 * @property \Cake\I18n\FrozenDate $otp_date
 * @property \Cake\I18n\FrozenDate $expected_completion_date
 * @property \Cake\I18n\FrozenDate $approval_date
 * @property \Cake\I18n\FrozenDate $vacant_possession_date
 * @property float $purchase_amount
 * @property float $booking_fee
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $updated
 *
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\Client $client
 */
class Property extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
