<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Project Entity
 *
 * @property int $id
 * @property int $developer_id
 * @property string $name
 * @property string $description
 * @property string $address
 * @property \Cake\I18n\FrozenDate $completion_date
 * @property int $percentage_completion
 * @property \Cake\I18n\FrozenDate $csc_date
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $updated
 *
 * @property \App\Model\Entity\Developer $developer
 * @property \App\Model\Entity\Milestone[] $milestones
 * @property \App\Model\Entity\Property[] $properties
 */
class Project extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
