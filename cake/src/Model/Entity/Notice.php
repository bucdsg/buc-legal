<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Notice Entity
 *
 * @property int $id
 * @property int $property_id
 * @property int $lawyer_id
 * @property string $notice_type
 * @property int $property_completion_id
 * @property int $recipient_id
 * @property string $recipient_entity
 * @property string $recipient_type
 * @property string $recipient_email
 * @property string $recipient_phone
 * @property \Cake\I18n\FrozenTime $last_sent
 * @property int $created_at
 *
 * @property \App\Model\Entity\Property $property
 * @property \App\Model\Entity\Lawyer $lawyer
 * @property \App\Model\Entity\PropertyCompletion $property_completion
 * @property \App\Model\Entity\Recipient $recipient
 * @property \App\Model\Entity\NoticeFile[] $notice_file
 */
class Notice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
