<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SysConfig Entity
 *
 * @property string $config_key
 * @property string $config_value
 */
class SysConfig extends Entity
{
    protected $_accessible = [
        '*' => true
    ];
}
