<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PropertyCompletion Entity
 *
 * @property int $id
 * @property int $property_id
 * @property int $completion_type_id
 * @property string $notes
 * @property \Cake\I18n\FrozenTime $scheduled_at
 * @property \Cake\I18n\FrozenTime $completed_at
 * @property \Cake\I18n\FrozenTime $created_at
 *
 * @property \App\Model\Entity\Property $property
 * @property \App\Model\Entity\CompletionType $completion_type
 */
class PropertyCompletion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
