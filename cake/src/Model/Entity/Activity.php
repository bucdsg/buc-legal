<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Activity Entity
 *
 * @property int $id
 * @property string $activity_type
 * @property int $project_id
 * @property int $property_id
 * @property int $client_id
 * @property int $user_id
 * @property int $developer_id
 * @property int $bank_id
 * @property int $notice_id
 * @property int $property_completion_id
 * @property string $activity_text
 * @property \Cake\I18n\FrozenTime $updated_at
 * @property \Cake\I18n\FrozenTime $created_at
 *
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\Property $property
 * @property \App\Model\Entity\Client $client
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Developer $developer
 * @property \App\Model\Entity\Bank $bank
 * @property \App\Model\Entity\Notice $notice
 * @property \App\Model\Entity\PropertyCompletion $property_completion
 */
class Activity extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
