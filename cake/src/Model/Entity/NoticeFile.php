<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NoticeFile Entity
 *
 * @property int $id
 * @property int $notice_id
 * @property string $attachment_type
 * @property string $attachment_file
 * @property string $attachment_url
 * @property \Cake\I18n\FrozenTime $created_at
 *
 * @property \App\Model\Entity\Notice $notice
 */
class NoticeFile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
