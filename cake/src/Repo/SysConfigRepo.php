<?php
namespace App\Repo;

use Cake\ORM\TableRegistry;

class SysConfigRepo {

    public function __construct()
    {
        $this->sysConfig = TableRegistry::get('SysConfig');
    }

    public function getConfig($configName = null)
    {
        if (is_array($configName)) {
            // multiple configs to be returned
            $result = $this->sysConfig
                ->find()
                ->where(['config_key IN' => $configName])
                ->all();

            $configs = [];

            foreach ($result as $item) {
              $configs[$item->config_key] = $item->config_value;
            }

            return (object)$configs;
        } else if (is_string($configName)) {
            // return single config item
            $config = $this->sysConfig
                ->find()
                ->where(['config_key' => $configName])
                ->first();
            return $config ? $config->config_value : null;
        } else if (is_null($configName)) {
            // multiple configs to be returned
            $result = $this->sysConfig
                ->find()
                ->all();

            $configs = [];

            foreach ($result as $item) {
              $configs[$item->config_key] = $item->config_value;
            }

            return (object)$configs;
        }
    }

    public function setConfig($key, $value)
    {
        $config = $this->sysConfig
            ->find()
            ->where([
                'config_key' => $key
            ])
            ->first();

        if ($config) {
            $config->config_value = $value;
            $this->sysConfig->save($config);
        } else {
            $config = $this->sysConfig->newEntity();
            $config->config_key = $key;
            $config->config_value = $value;
            $this->sysConfig->save($config);
        }
    }
}
