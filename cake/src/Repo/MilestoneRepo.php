<?php
namespace App\Repo;

use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class MilestoneRepo {

    protected $client;

    protected $errorMessage;

    public function __construct()
    {
        $this->propertyCompletions = TableRegistry::get('PropertyCompletions');
        $this->propertyCompletionPayments = TableRegistry::get('PropertyCompletionPayments');
    }

    public function getByCompletionId($id)
    {
        try {
            $this->errorMessage = '';
            $completionItem = $this->propertyCompletions
                ->find()
                ->where(['id' => $id])
                ->first();

            return $completionItem;
        } catch (\Exception $e) {
            $this->errorMessage = $e->getMessage();
            return null;
        }
    }

    public function updateCompletion($completionItem)
    {
        $this->propertyCompletions->save($completionItem);
    }

    public function upsertMultiple($records)
    {
        foreach ($records as $cond) {
            $this->upsert($cond);
        }
    }

    public function upsert($record)
    {
        $foundRecord = $this->propertyCompletions
            ->find()
            ->where([
                'property_id' => $record->property_id,
                'completion_type_id' => $record->completion_type_id
            ])
            ->first();

        $updateCols = [
            'letter_date', 'letter_received_date', 'payment_action_date', 'payment_due_date', 'due_date_to_developer',
            'notes'
        ];

        if ($foundRecord) {
            // just update the record
            foreach ($updateCols as $col) {
                $newVal = (isset($record->$col)) ? $record->$col : '';
                $foundRecord->$col = $newVal;
            }

            $foundRecord->status = 'pending';

            $this->propertyCompletions->save($foundRecord);
        } else {
            // create new record
            $completionItem = $this->propertyCompletions->newEntity();
            $completionItem->property_id = $record->property_id;
            $completionItem->completion_type_id = $record->completion_type_id;
            $completionItem->status = 'pending';

            foreach ($updateCols as $col) {
                $newVal = (isset($record->$col)) ? $record->$col : '';
                $completionItem->$col = $newVal;
            }

            $this->propertyCompletions->save($completionItem);

            // create notification record
        }
    }

    public function newCompletionDate($propertyId, $completionTypeId, $completionDate, $scheduledDate = null)
    {
        $completionItem = $this->propertyCompletions->newEntity();

        $completionItem->property_id = $propertyId;
        $completionItem->completion_type_id = $completionTypeId;
        $completionItem->completed_at = $completionDate;
        $completionItem->scheduled_at = $scheduledDate;

        if ($this->propertyCompletions->save($completionItem)) {
            return $completionItem;
        }

        return null;
    }

    public function getByCallback($fn)
    {
        return $fn($this->propertyCompletions);
    }

    public function getUnpaid()
    {
        return $this->propertyCompletions
            ->find()
            ->contain([
                'Properties',
                'Properties.Projects',
                'Properties.Clients',
                'PropertyCompletionTypes',
                'Notices',
                'CompletionPayments'
            ])
            ->where(['status' => 'pending'])
            ->order(['payment_due_date' => 'ASC'])
            ->all()
            ->toArray();
    }

    public function setStagePaymentDate($stageCompletionId, $paymentDate)
    {
        $completionItem = $this->getByCompletionId($stageCompletionId);

        if (empty($completionItem)) {
            return false;
        }

        // set completion payments record
        $completionPayments = $this->propertyCompletionPayments
            ->query()
            ->update()
            ->set(['status' => 'paid'])
            ->where(['property_completion_id' => $completionItem->id])
            ->execute();

        $completionItem->status = 'paid';
        $this->propertyCompletions->save($completionItem);
    }
}
