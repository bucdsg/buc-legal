<?php
namespace App\Repo;

use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class PropertyCompletionRepo {

    protected $propertyCompletions;

    protected $errorMessage;

    public function __construct()
    {
        $this->propertyCompletions = TableRegistry::get('PropertyCompletions');
    }

    public function getById($id)
    {
        $completionItem = $this->propertyCompletions
            ->find()
            ->where(['PropertyCompletions.id' => $id])
            ->contain([
                'Properties',
                'Properties.Projects',
                'Properties.Clients',
                'PropertyCompletionTypes',
                'CompletionPayments',
                'Notices'
            ])
            ->first();

        if (empty($completionItem)) {
            return null;
        }

        return $completionItem;
    }

    public function StagesForPaymentNotification($lastNotifDate)
    {
        return $this->propertyCompletions
            ->find()
            ->where([
                'payment_action_date <' => $lastNotifDate,
                'status IN' => ['created', 'pending']
            ])
            ->contain([
                'Properties',
                'Properties.Projects',
                'Properties.Clients',
                'PropertyCompletionTypes',
                'CompletionPayments',
                'Notices'
            ])
            ->all();
    }
}
