<?php
namespace App\Repo;

use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

use App\Repo\PropertyRepo;

class NoticeRepo {

    protected $notice;

    protected $propertyRepo;

    protected $errorMessage;

    public function __construct()
    {
        $this->notice = TableRegistry::get('Notices');
        $this->propertyRepo = new PropertyRepo;
    }

    public function getById($id)
    {
        if (empty($id)) {
            return null;
        }

        $notice = $this->notice
            ->find()
            ->where(['Notices.id' => $id])
            ->contain(['Files'])
            ->first();

        if (empty($notice)) {
            return null;
        }

        return $notice;
    }

    public function newStageCompletionNotice($data)
    {
        $notice = $this->notice->newEntity();

        // convert parameter to object
        $data = (object)$data;

        $property = $this->propertyRepo->getById($data->property_id);

        if (!$property) {
            return false;
        }

        $notice->notice_type = 'stage_completion';

        $notice->property_id = $property->id;
        $notice->recipient_id = $property->client_id;
        $notice->recipient_email = $property->client->email;

        $notice->recipient_entity = $data->recipient_entity;
        $notice->recipient_type = $data->recipient_type;
        $notice->property_completion_id = $data->stage_completion->id;

        // TODO: get lawfirm settings for how many days allowance
        $notice->resolve_until = $data->stage_completion->completed_at->modify('+14 days');;

        // dd($notice);

        /* $data = [
            'property_id' => '',
            'lawyer_id' => '',
            'notice_type' => 'email',
            'recipient_id' => '', // client_id, bank_id, cpf_id
            'recipient_type' => 'client', // client, bank, cpf
            'recipient_email' => '',
            'recipient_phone' => ''
        ]; */

        if ($this->notice->save($notice)) {
            return $notice;
        }

        return false;
    }

    public function setlastSent($noticeId, $lastSent)
    {
        try {
            $notice = $this->notice->get($noticeId);

            $notice->last_sent = $lastSent;
            $this->notice->save($notice);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function markResolve($noticeId, $date = 'now')
    {
        try {
            $notice = $this->notice->get($noticeId);

            if ($date == 'now') {
                $date = new Time();
            }

            $notice->resolved_at = $date;
            $this->notice->save($notice);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
