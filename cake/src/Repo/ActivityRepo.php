<?php
namespace App\Repo;

use Cake\ORM\TableRegistry;

class ActivityRepo {

    protected $activity;

    protected $errorMessage;

    public function __construct()
    {
        $this->activity = TableRegistry::get('Activities');
    }

    public function getError()
    {
        return $this->errorMessage;
    }

    public function getActivities($data)
    {
        if (empty($data->object) || empty($data->id)) {
            $this->errorMessage = 'Invalid data';
            return false;
        }

        switch ($data->object) {
            case 'client':
                $contains = ['Clients'];
                $objectKey = 'client_id';
                break;

            default:
                $objectKey = null;
                break;
        }

        if (!$objectKey) {
            $this->errorMessage = 'Invalid id';
            return false;
        }

        $this->errorMessage = null;
        $activities = $this->activity
                    ->find()
                    ->where([$objectKey => $data->id])
                    ->contain($contains)
                    ->all();

        return $activities;
    }

    public function saveActivity($data)
    {
        if (empty($data->activityObject) || !is_object($data->activityObject)) {
            $this->errorMessage = 'Invalid data';
            return false;
        }

        switch ($data->activityObject->object) {
            case 'client':
                $objectKey = 'client_id';
                break;

            default:
                $objectKey = null;
                break;
        }

        $activity = $this->activity->newEntity();

        if ($objectKey) {
            $activity->$objectKey = $data->activityObject->id;
        }

        $activity->activity_type = $data->type;
        $activity->activity_text = $data->details;
        $activity->due_date = empty($data->due_date) ? null : $data->due_date;

        $this->activity->save($activity);

        return true;
    }

    public function getActivity($cond)
    {
        $query = $this->activity->find();
        $result = $query
            ->where($cond)
            ->all();

        return $result;
    }
}
