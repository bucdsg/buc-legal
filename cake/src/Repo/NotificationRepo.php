<?php
namespace App\Repo;

use Cake\ORM\TableRegistry;

class NotificationRepo {

    public function __construct()
    {
        $this->notification = TableRegistry::get('Notifications');
    }

    public function getNotifications($conditions)
    {
        if (!$conditions) {
            return null;
        }

        return $this->notification
            ->find()
            ->where($conditions)
            ->all();
    }

    public function save($notification)
    {
        $newNotif = $this->notification->newEntity();

        foreach ($notification as $key => $value) {
            $newNotif->$key = $value;
        }

        return $this->notification->save($newNotif);
    }
}
