<?php
namespace App\Repo;

use Cake\ORM\TableRegistry;

class StatsRepo {

    protected $notice;

    protected $errorMessage;

    public function __construct()
    {
        $this->notice = TableRegistry::get('Notices');
        $this->properties = TableRegistry::get('properties');
    }

    public function getError()
    {
        return $this->errorMessage;
    }

    public function getStagesSummaryByProjectId($projectId)
    {
        $query = $this->properties->find();
        /* $result = $query
            ->select([
                'project_id',
                'name',
                'stage_id' => 'PropertyCompletions.completion_type_id',
                'stage_status' => 'PropertyCompletions.status'
            ])
            // ->select(['completion_count' => $query->func()->count('*')])
            ->leftJoin(['PropertyCompletions' => 'property_completions'])
            // ->group(['project_id', 'PropertyCompletions.completion_type_id', 'PropertyCompletions.status'])
            ->where(['project_id' => $projectId])
            ->order(['PropertyCompletions.completion_type_id'])
            ->all();

        return $result; */

        $properties = $query
            ->contain(['PropertyCompletions', 'PropertyCompletions.CompletionPayments'])
            ->where(['project_id' => $projectId])
            ->all();

        $stagesIdStatuses = [];

        // init stages
        for ($i = 1; $i <= 8; $i++) {
            $stagesIdStatuses[$i] = [
                'created' => 0,
                'pending' => 0,
                'paid' => 0
            ];
        }

        foreach ($properties as $property) {
            foreach ($property->property_completions as $property_completion) {
                $stageId = $property_completion->completion_type_id;
                $status = $property_completion->status;

                if ($stageId > 8) {
                    continue;
                }

                if (array_key_exists($status, $stagesIdStatuses[$stageId])) {
                    $stagesIdStatuses[$stageId][$status] += 1;
                } else {
                    $stagesIdStatuses[$stageId][$status] = 1;
                }
            }
        }

        return $stagesIdStatuses;
    }

    public function getNotificationsToSend()
    {
        $query = $this->notice->find();
        $result = $query
            ->contain(['Property', 'Property.Projects', 'StageCompletion'])
            ->where(['last_sent IS' => null])
            ->all();

        return $result;
    }

    public function getNotificationsToResolve()
    {
        $query = $this->notice->find();
        $result = $query
            ->contain(['Property', 'Property.Projects', 'StageCompletion'])
            ->where(['last_sent IS NOT' => null, 'resolved_at IS' => null])
            ->all();

        return $result;
    }
}
