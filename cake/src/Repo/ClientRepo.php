<?php
namespace App\Repo;

use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class ClientRepo {

    protected $client;

    protected $errorMessage;

    public function __construct()
    {
        $this->client = TableRegistry::get('Clients');
    }

    public function getError()
    {
        return $this->errorMessage;
    }

    public function getClients()
    {
        $clients = $this->client
            ->find('all')
            ->toArray();

        return $clients;

    }

    public function getById($id)
    {
        if (empty($id)) {
            return null;
        }

        $client = $this->client
            ->find()
            ->where(['id' => $id])
            ->contain([
                'Properties',
                'Properties.Projects'
            ])
            ->first();

        if (empty($client)) {
            return null;
        }

        return $client;
    }

    public function edit($id,$data)
    {
        $client = $this->client->get($id);

        $client = $this->client->patchEntity($client, $data);
        if ($this->client->save($client)) {
           $response = $client;
        } else {
            $response = null;
        }

        return $response;

    }


}
