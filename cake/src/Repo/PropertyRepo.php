<?php
namespace App\Repo;

use Cake\ORM\TableRegistry;

class PropertyRepo {

    protected $project;

    protected $property;

    protected $developer;

    protected $errorMessage;

    public function __construct()
    {
        $this->project = TableRegistry::get('Projects');
        $this->property = TableRegistry::get('Properties');
        $this->developer = TableRegistry::get('Developers');
        $this->client = TableRegistry::get('Clients');
        $this->propertyCompletions = TableRegistry::get('PropertyCompletions');
    }

    public function getError()
    {
        return $this->errorMessage;
    }

    public function getById($id)
    {
        if (empty($id)) {
            return null;
        }

        $property = $this->property
            ->find()
            ->where(['Properties.id' => $id])
            ->contain([
                'Projects',
                'Clients',
                'PropertyCompletions',
                'PropertyCompletions.CompletionPayments',
                'PropertyCompletions.PropertyCompletionTypes'
            ])
            ->first();

        if (empty($property)) {
            return null;
        }

        return $property;
    }

    public function assignNewClient($propertyId, $data)
    {
        try {
            $client = $this->client->newEntity();
            $client->name = $data['name'];
            $client->address = $data['address'];
            $client->lawyer_id = 0;

            if ( ! $this->client->save($client)) {
                if (is_array($client->errors())) {
                    foreach ($client->errors() as $key => $error) {
                        foreach ($error as $message) {
                            $this->errorMessage[] = implode(':', [$key, $message]);
                        }
                    }
                }
                $this->errorMessage = implode('', $this->errorMessage);
                return false;
            }

            $property = $this->property->get($propertyId);

            if (empty($property)) {
                $this->errorMessage = 'Property ' . $propertyId . ' not found';
                return false;
            }

            $property->client_id = $client->id;
            $property->purchase_amount = $data['purchase_amount'];
            $property->cash_amount = $data['cash_amount'];
            $property->loan_amount = $data['loan_amount'];
            $property->cpf_1_name = $data['cpf1_name'];
            $property->cpf_2_name = $data['cpf2_name'];
            $property->cpf_3_name = $data['cpf3_name'];
            $property->cpf_1_amount = $data['cpf1_amount'];
            $property->cpf_2_amount = $data['cpf2_amount'];
            $property->cpf_3_amount = $data['cpf3_amount'];

            $this->property->save($property);

            //create stage completion to this property and set status = 'created'
            /**
            $stage ->
                [id] => 1
                [name] => completion of foundation work
                [percentage] => 0.1
                [status] => pending
                [amount] => 100000
                [cashAmount] => 0
                [loanAmount] => 100000
                [cpf1Amount] => 0
                [cpf2Amount] => 0
                [cpf3Amount] => 0
            */
            $modesOfPayment = [
                'cashAmount' => 'cash',
                'loanAmount' => 'loan',
                'cpf1Amount' => 'cpf1',
                'cpf2Amount' => 'cpf2',
                'cpf3Amount' => 'cpf3'
            ];

            if (is_array($data['stages'])) {
                foreach ($data['stages'] as $stage) {
                    $completionPayments = [];

                    foreach ($modesOfPayment as $paymentKey => $paymentMode) {
                        if (isset($stage->$paymentKey) && $stage->$paymentKey > 0) {
                            $completionPayments[] = [
                                'mode_of_payment' => $paymentMode,
                                'amount' => $stage->$paymentKey,
                                'status' => 'awaiting_payment'
                            ];
                        }
                    }

                    // save property stage and payment items
                    $propertyStage = $this->propertyCompletions->newEntity(
                        [
                            'property_id' => $property->id,
                            'completion_type_id' => $stage->id,
                            'amount' => $stage->amount,
                            'status' => 'created',
                            'completion_payments' => $completionPayments
                        ],
                        [
                            'associated' => ['CompletionPayments']
                        ]
                    );

                    $this->propertyCompletions->save($propertyStage);
                }
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}
