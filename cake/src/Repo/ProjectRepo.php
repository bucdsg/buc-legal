<?php
namespace App\Repo;

use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class ProjectRepo {

    protected $project;

    protected $property;

    protected $errorMessage;

    public function __construct()
    {
        $this->project = TableRegistry::get('Projects');
        $this->property = TableRegistry::get('Properties');
        $this->developer = TableRegistry::get('Developers');
        $this->client = TableRegistry::get('Clients');
        $this->propertyCompletions = TableRegistry::get('PropertyCompletions');
        $this->completionTypes = TableRegistry::get('PropertyCompletionTypes');
    }

    public function getError()
    {
        return $this->errorMessage;
    }

    public function getByDeveloperId($developerId)
    {
        if (empty($developerId)) {
            return [];
        }

        $query = $this->project->find();

        $clientCountByProject = $this->getClientCountByProjectId();

        $projects = $query
            ->select(['property_count' => $query->func()->count('Properties.id')])
            ->select($this->project)
            ->leftJoin(
                ['Properties' => 'properties'],
                [
                    'Properties.project_id = Projects.id'
                ]
            )
            ->where(['developer_id' => $developerId])
            ->group('Projects.id')
            ->all()
            ->toArray();

        $result = [];
        foreach ($projects as $project) {
            $clientCount = 0;
            if (array_key_exists($project->id, $clientCountByProject)) {
                $clientCount = $clientCountByProject[$project->id]->property_count;
            }
            $project->client_count = $clientCount;
            $result[] = $project;
        }

        return $result;
    }

    private function getClientCountByProjectId()
    {
        $query = $this->property->find();

        $properties = $query
            ->select('project_id')
            ->select(['property_count' => $query->func()->count('*')])
            ->where(['client_id >' => 0])
            ->group('project_id')
            ->all();

        $grouped = [];

        foreach ($properties as $property) {
            $grouped[$property->project_id] = (object)$property;
        }

        return $grouped;
    }

    public function getById($id)
    {
        if (empty($id)) {
            return null;
        }

        return $this->project
            ->find()
            ->where(['id' => $id])
            ->contain([
                'Properties',
                'Properties.Clients',
                'Properties.PropertyCompletions',
                'Properties.PropertyCompletions.Notices',
                'Properties.PropertyCompletions.Notices.Files'
            ])
            ->first();
    }

    public function saveById($id, $data)
    {
        // TODO: change this to user preference in the imap_fetchstructure(imap_stream, msg_number)
        $dateTimeFormat = 'dd/MM/yyyy';
        $project = $this->project
            ->find()
            ->where(['id' => $id])
            ->first();

        if (!$project) {
            return null;
        }

        $dateKeys = ['csc_date', 'completion_date'];

        foreach ($data as $key => $value) {
            if (in_array($key, $dateKeys)) {
                $value = Time::parseDateTime($value, $dateTimeFormat);
            }
            $project->$key = $value;
        }

        $this->project->save($project);

        return true;
    }

    public function createNewProject($data)
    {
        $this->errorMessage = [];

        try {
            $project = $this->project->newEntity();

            $project->developer_id = $data['developer_id'];
            $project->name = $data['project_name'];
            $project->description = $data['project_description'];
            $project->address = $data['project_address'];

            if ( ! $this->project->save($project)) {
                if (is_array($project->errors())) {
                    foreach ($project->errors() as $key => $error) {
                        foreach ($error as $message) {
                            $this->errorMessage[] = implode(':', [$key, $message]);
                        }
                    }
                }
                $this->errorMessage = implode('', $this->errorMessage);
                return false;
            }

            foreach ($data['unit_mapping'] as $floorIdx => $floor) {
                $floorName = $floor->floorName;

                foreach ($floor->units as $unitIdx => $unit) {
                    $property = $this->property->newEntity();

                    $property->project_id = $project->id;
                    $property->client_id = 0;
                    $property->name = $unit;
                    $property->floor_order = $floorIdx;
                    $property->floor_name = $floorName;
                    $property->property_order = $unitIdx;

                    if (!$this->property->save($property) && is_array($project->errors())) {
                        foreach ($property->errors() as $key => $error) {
                            foreach ($error as $message) {
                                $this->errorMessage[] = implode(':', [$key, $message]);
                            }
                        }
                    }
                }
            }

            if (count($this->errorMessage)) {
                $this->errorMessage = implode('', $this->errorMessage);
            }

            return true;
        } catch (\Exception $e) {
            $this->errorMessage = implode('', $this->errorMessage);
            $this->errorMessage .= ' ' . $e->getMessage();
            return false;
        }

        return true;
    }

    public function getCompletionTypes()
    {
        return $this->completionTypes
            ->find()
            ->all();
    }

    public function completionItemsByProperty($projectId)
    {
        return $this->property
            ->find()
            ->where(['Properties.project_id' => $projectId, 'Properties.client_id >' => '0'])
            ->contain([
                'Clients',
                'Projects',
                'PropertyCompletions',
                'PropertyCompletions.CompletionPayments'
            ])
            ->all();
    }

    public function newCompletionItem($projectId, $data)
    {
        try {
            $this->errorMessage = '';
            $completionItem = $this->completionTypes
                ->find()
                ->where(['id' => $data['completion_id']])
                ->first();

            if (empty($completionItem)) {
                throw new \Exception('Invalid completion item');
            }

            $properties = $this->property->find()
                ->where(['id IN' => $data['property_ids']])
                ->contain('PropertyCompletions')
                ->all();

            if (empty($properties)) {
                throw new \Exception('Invalid property ids');
            }

            $notSame = $this->isPropertiesFromSingleProject($properties);

            if (!$notSame) {
                throw new \Exception('Properties should come in a single project');
            }

            return $this->saveCompletionItemToProperties($completionItem, $properties);
        } catch (\Exception $e) {
            $this->errorMessage = $e->getMessage();
            return false;
        }
    }

    private function isPropertiesFromSingleProject($properties)
    {
        $properties = $properties->toArray();
        for ($i = 0; $i < count($properties) - 1; $i++) {
            if ($properties[$i]->project_id !== $properties[$i + 1]->project_id) {
                return false;
            }
        }

        return true;
    }

    private function saveCompletionItemToProperties($completionItem, $properties)
    {
        try {
            foreach ($properties as $property) {
                $propertyCompletion = $this->propertyCompletions->newEntity();

                $propertyCompletion->property_id = $property->id;
                $propertyCompletion->completion_type_id = $completionItem->id;
                $propertyCompletion->completed_at = Time::now();
                $res = $this->propertyCompletions->save($propertyCompletion);
            }

            return true;
        } catch (\Exception $e) {
            $this->errorMessage = $e->getMessage();
            return false;
        }
    }
}
