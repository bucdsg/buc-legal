<?php
namespace App\Repo;

use Cake\ORM\TableRegistry;

class SettingsRepo {

    protected $userSettings;

    protected $defaultSettings;

    protected $errorMessage;

    public function __construct()
    {
        $this->userSettings = TableRegistry::get('UserSettings');
        $this->documentTemplates = TableRegistry::get('DocumentTemplates');

        $this->defaultSettings = [
            'date_format_720kb_datepicker' => 'dd/MM/yyyy',
            'stage_completion_action_days_from_payment_due_date' => '4',
            'payment_due_days_from_due_date_to_developer' => '5',
            'due_days_to_developer_from_payment' => '14',
        ];
    }

    public function getError()
    {
        return $this->errorMessage;
    }

    /**
     * @param $userId Integer
     */
    public function getUserSettings($userId)
    {
        $query = $this->userSettings->find();
        $settings = [];
        $result = $query
            ->where(['user_id' => $userId])
            ->all();

        foreach ($result as $item) {
            $settings[$item['key']] = $item['value'];
        }

        $settings = array_merge($this->defaultSettings, $settings);

        return $settings;
    }

    /**
     * @param $userId Integer
     * @param $settings Array
     */
    public function saveUserSettings($userId, $settings)
    {
        $newSettings = [];
        $currentSettings = [];

        foreach ($settings as $key => $value) {
            $newSettings[] = [
                'user_id' => $userId,
                'key' => $key,
                'value' => $value
            ];
        }

        $currentSettings = $this->userSettings
            ->find()
            ->select(['user_id', 'key', 'value'])
            ->where(['user_id' => $userId])
            ->toArray();

        $patched = $this->userSettings->patchEntities($currentSettings, $newSettings);

        foreach ($patched as $entity) {
            $this->userSettings->save($entity);
        }

        return true;
    }

    /**
     * @param $userId Integer
     */
    public function getDocumentTemplates($userId, $templateKey = null)
    {
        $query = $this->documentTemplates->find();
        $templates = [];
        $result = $query
            ->select([
                'template_file',
                'template_html',
                'template_soure',
                'template_type',
                'file' => 'template_file',
                'html' => 'template_html',
                'soure' => 'template_soure',
                'type' => 'template_type'
            ])
            ->where(['user_id' => $userId])
            ->all();

        if ($templateKey) {
            foreach ($result as $template) {
                if ($template->type == $templateKey) {
                    return $template;
                }
            }

            return null;
        }

        return $result;
    }


    public function setDocumentTemplates($userId, $templates)
    {
        $newTemplates = [];
        $currentTemplates = [];

        // format templates
        foreach ($templates as $type => $html) {
            $newTemplates[] = (object)[
                'user_id' => $userId,
                'template_type' => $type,
                'template_html' => $html
            ];
        }

        foreach ($newTemplates as $newTemplate) {
            // get current template by key
            $currentTemplate = $this->documentTemplates
                ->find()
                ->where(['user_id' => $userId, 'template_type' => $newTemplate->template_type])
                ->first();

            if ($currentTemplate) {
                // just update
                $currentTemplate->template_html = $newTemplate->template_html;
                $currentTemplate->template_soure = 'html';
                $this->documentTemplates->save($currentTemplate);
            } else {
                // save new record
                $newEntity = $this->documentTemplates->newEntity();
                $newEntity->user_id = $userId;
                $newEntity->template_soure = 'html';
                $newEntity->template_type = $newTemplate->template_type;
                $newEntity->template_html = $newTemplate->template_html;
                $this->documentTemplates->save($newEntity);
            }
        }

        return true;
    }

    public function setDocumentTemplateFile($userId, $templateKey, $templateFile)
    {
        // check for existing record
        $currentTemplate = $this->documentTemplates
            ->find()
            ->where(['user_id' => $userId, 'template_type' => $templateKey])
            ->first();

        if ($currentTemplate) {
            // just update
            $currentTemplate->template_soure = 'file';
            $currentTemplate->template_file = $templateFile;
            $this->documentTemplates->save($currentTemplate);
        } else {
            // save new record
            $newEntity = $this->documentTemplates->newEntity();
            $newEntity->user_id = $userId;
            $newEntity->template_type = $templateKey;
            $newEntity->template_soure = 'file';
            $newEntity->template_file = $templateFile;

            $this->documentTemplates->save($newEntity);
        }

        return true;
    }
}
