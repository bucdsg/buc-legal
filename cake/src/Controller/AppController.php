<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
            ],
            'authError' => 'Please login to continue',
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email'],
                    'passwordHasher' => [
                        'className' => 'Fallback',
                        'hashers' => [
                            'Default',
                            'Weak' => ['hashType' => 'bcrypt'] // support laravel authentication
                        ]
                    ]
                ]
            ],
            'storage' => 'Session'
        ]);

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        if (isset($this->params['prefix']) && $this->params['prefix'] == 'api') {
            $this->layout = 'raw';
        }
    }

    public function angularIndex()
    {
        $this->viewBuilder()->layout(false);
        $this->viewBuilder()->templatePath('Pages'); // this
        $this->viewBuilder()->template('angular_index');
    }

    protected function jsend($data, $status = 'success', $message = null, $details = null)
    {
        // clean our data before converting to a json file
        // remove bom (utf-8 issue) http://bit.ly/2sA4oZL
        if (is_array($data) || is_object($data)) {
            array_walk_recursive($data, function (&$value, $index) {
                if (is_string($value)) {
                    $value = str_replace(array(
                        "\xA0", // http://bit.ly/2sXIheW
                    ), '' , $value);
                }
            });
        }

        if (!is_string($status)) {
            $status = !!$status ? 'success' : 'error';
        }

        $result = [
            'status' => $status,
            'data' => $data
        ];

        if ($message) {
            $result['message'] = $message;
        }

        if ($details) {
            $result['details'] = $details;
        }

        $this->response->statusCode(200);
        $this->response->type('application/json');
        $this->response->body(json_encode($result));

        return $this->response;
    }
}
