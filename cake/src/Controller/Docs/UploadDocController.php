<?php

namespace App\Controller\Docs;

use App\Controller\AppController;

use App\Repo\SettingsRepo;

class UploadDocController extends AppController
{
    protected $SettingsRepo;

    public function initialize()
    {
        $this->autoRender = false;

        $this->SettingsRepo = new SettingsRepo;

        $this->docTypeFuncMapping = [
            'template_document' => 'processTemplateDoc'
        ];
    }

    public function upload()
    {
        if ($this->request->is('post')) {
            // upload code here
            $this->processUploadedDocument();
        } else {
            // dummy upload page
            $this->dummyUploadPage();
        }
    }

    public function processUploadedDocument()
    {
        $request = (object)$_REQUEST;
        $docType = $request->doc_type;
        $docVars = $request->doc_vars;

        // check if we have a corresponding function that will handle this kind of doc
        if (!$docType || !array_key_exists($docType, $this->docTypeFuncMapping)) {
            return $this->jsend([], 'error', sprintf('Invalid doc type ' . $docType));
        }

        $funcName = $this->docTypeFuncMapping[$docType];

        try {
            $result = $this->$funcName($docVars);
            return $this->jsend($result);
        } catch (\Exception $e) {
            return $this->jsend([], 'error', sprintf('Error: %s', $e->getMessage()));
        }
    }

    /**
     * @link https://gist.github.com/taterbase/2688850
     */
    public function processTemplateDoc($vars)
    {
        // check if there is correct template key
        if (empty($vars['template_key'])) {
            throw new \Exception('Invalid Template Type');
        }

        if (empty($_FILES['file'])) {
            throw new \Exception('Unable to locate file');
        }

        // check if file is successfully uploaded
        if ($_FILES['file']['error'] !== UPLOAD_ERR_OK) {
            throw new \Exception($_FILES['file']['error']);
        }

        // get file info
        $finfo = finfo_open(FILEINFO_MIME_TYPE);

        // get file type
        $fmime = finfo_file($finfo, $_FILES['file']['tmp_name']);

        $allowedMimes = [
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx'
        ];

        // only allow ms word document
        if (!array_key_exists($fmime, $allowedMimes)) {
            throw new \Exception('Invalid File Type');
        }

        // get appropriate extension
        $fext = $allowedMimes[$fmime];

        // generate random string for filename
        $rand = substr(md5(microtime()),rand(0,26),10);

        // build the new filename
        $filename = $vars['template_key'] . '-' . $rand . $fext;

        // build the document path
        // TODO: Make the org 1 dynamic or check current user
        $path = implode(DS, [
            dirname(ROOT), 'uploads', 'document_templates',
            'org1',
            ''
        ]);

        // check if path exist
        if (!is_dir($path)) {
            mkdir($path, 0700, true);
        }

        if (move_uploaded_file($_FILES['file']['tmp_name'], $path . $filename)) {
            // file uploaded, save details to db
            $this->saveDocumentToUserSettings($vars['template_key'], $filename);
        } else{
            throw new \Exception('Unable to upload document');
        }

        return [
            'success' => true,
            'filename' => $filename
        ];
    }

    private function saveDocumentToUserSettings($templateKey, $filename)
    {
        // $userId = $this->request->session()->read('Auth.User.id');
        // temporary for demo
        // TODO: Change this user id to current user id
        $userId = 1;

        if (empty($userId)) {
            return false;
        }

        $this->SettingsRepo->setDocumentTemplateFile($userId, $templateKey, $filename);
        return true;
    }

    public function dummyUploadPage()
    {
        $html = <<<HTML
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Upload doc test</title>
</head>
<body>
    <form method="POST" enctype="multipart/form-data">
        <h2>Upload doc test</h2>
        <p>.doc and .docx only</p>
        <input type="hidden" name="doc_type" value="template_document">
        <input type="text" name="doc_vars[template_key]" value="notice_to_client">
        <input type="file" name="file">
        <br />
        <input type="submit">
    </form>
</body>
</html>
HTML;

        echo $html;
        die();
    }
}
