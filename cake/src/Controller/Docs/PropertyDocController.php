<?php

namespace App\Controller\Docs;

use App\Controller\AppController;
use App\Repo\PropertyRepo;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Html;

use App\Repo\PropertyCompletionRepo;
use App\Repo\SettingsRepo;

class PropertyDocController extends AppController
{
    protected $ProjectCompletion;

    public function initialize()
    {
        $this->ProjectCompletion = new PropertyCompletionRepo;
        $this->SettingsRepo = new SettingsRepo;
        $this->autoRender = false;
        $this->userId = 1;

        // https://github.com/PHPOffice/PHPWord/issues/754#issuecomment-302896787
        libxml_use_internal_errors(true);
    }

    public function milestoneList()
    {
        try {
            // create doc
            $phpWord = new PhpWord();
            $section = $phpWord->addSection();
            $section->addText('Test Document');

            // check dir
            if (!is_dir('docs')) {
                mkdir('docs');
            }

            // save to temp dir
            $docfile = 'docs/testdoc.docx';
            $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
            $objWriter->save($docfile);

            // echo to browser
            $fp = fopen($docfile, 'rb'); // open the file in a binary mode

            header("Content-Type: application/vnd.openxmlformats-package.relationships+xml"); // send the right headers
            header("Content-Length: " . filesize($docfile));
            header('Content-Disposition: attachment; filename="testdoc.docx"');

            // dump the picture and stop the script
            fpassthru($fp);
            exit;
        } catch (Exception $e) {
            echo 'Unable to retreive document';
        }
    }

    public function noticeToBank($id = null)
    {
        if (empty($id)) {
            die('missing id');
        }

        $this->doc_template_key = 'notice_to_bank';
        $this->stageCompletionNotice($id);
    }

    public function noticeToCPF($id = null)
    {
        if (empty($id)) {
            die('missing id');
        }

        $this->doc_template_key = 'notice_to_cpf';
        $this->stageCompletionNotice($id);
    }

    public function stageCompletionNotice($id = null)
    {
        // $userId = $this->request->session()->read('Auth.User.id');
        // temporary for demo
        // TODO: add check for org id

        $completionItem = $this->ProjectCompletion->getById($id);

        if (empty($completionItem)) {
            die('Invalid document');
            exit;
        }

        // TODO: add checking if user owns the document

        $doc_template_key = 'notice_to_client';

        if (!empty($this->doc_template_key)) {
            $doc_template_key = $this->doc_template_key;
        }

        $template = $this->SettingsRepo->getDocumentTemplates($this->userId, $doc_template_key);

        if (empty($template)) {
            die('Template not set');
            exit;
        }

        $amount = floatval($completionItem->amount);
        $amount = $amount ? '$' . number_format($amount, 2) : '';
        function _parseDate($date) {
            return $date ? $date->format('F j, Y') : '';
        }

        // FOR NOTICE TO CPF ONLY
        // TODO: MOVE THIS SOMEWHERE
        if ('notice_to_cpf' == $doc_template_key) {
            $noticeToCpfTable = <<<HTML
<table>
    <tr>
        <th>Chargor(s)</th>
        <th>Lumpsum Payment of Purchase Price</th>
        <th>Purchase Stamp Duty</th>
        <th>Mortgage Stamp Duty &amp; Legal Fee</th>
        <th>Housing Loan Monthly Installment Payment</th>
        <th>Bridging Loan Repayment</th>
        <th>Short Term Loan Repayment</th>
    </tr>
    <tr>
        <td>\${client_name}</td>
        <td>\${amount}</td>
        <td>$</td>
        <td>$</td>
        <td>$</td>
        <td>$</td>
        <td>$</td>
    </tr>
</table>
HTML;
            $template->html = str_replace('%{chargors_table_calc}', $noticeToCpfTable, $template->html);
        }


        $docVars = array(
            'date' => date('F j, Y'),
            'stage_id' => $completionItem->property_completion_type->id,
            'amount' => $amount,
            'client_name' => $completionItem->property->client->name,
            'client_email' => $completionItem->property->client->email,
            'client_address' => $completionItem->property->client->address,
            'project_name' => $completionItem->property->project->name,
            'project_address' => $completionItem->property->project->address,
            'unit_name' => $completionItem->property->name,
            'developer_due_date' => _parseDate($completionItem->due_date_to_developer),
            'developer_solicitor_letter_date' => _parseDate($completionItem->letter_date),
            'payment_due_date' => _parseDate($completionItem->payment_due_date),
            'completion_name' => $completionItem->property_completion_type->name,
            'completion_description' => $completionItem->property_completion_type->description,
            'completed_on' => _parseDate($completionItem->completed_at)
        );

        $options = [
            'exportName' => $doc_template_key
        ];

        $this->docMerge($template, $docVars, $options);
    }

    public function processTempalteFile($template, $vars)
    {
        $docFile = 'docs/randomfilename'; //temp filename
        $filename = 'notice_to_bla';

        return (object)[
            'doc_file' => $docFile,
            'filename' => $filename
        ];
    }

    public function unitPreCalculation($id = null)
    {
        $template = $this->SettingsRepo->getDocumentTemplates($this->userId, 'unit_pre_calculation');

        if (empty($template)) {
            die('Template not set');
        }

        $docVars = [
            'property_name' => 'Prop name',
            'client_name' => 'Kevin',
            'solicitor_name' => 'Solicitor 1',
            'purchase_price' => '$1,000,000.00',
            'stamp_fee' => '$123.00',
            'bank_cost_contribution' => '$',
            'cpf_cost' => '$',
            'purchaser_cost' => '$',
            'date_option_cash' => '',
            'date_completion' => ''
        ];

        $callbacks = [];

        $callbacks['after_html'] = function (&$phpWord, &$section) {
            $tableStyle = ['borderSize' => 1, 'borderColor' => 'd2d2d2'];
            $phpWord->addTableStyle(
                'Purchaser Table',
                $tableStyle,
                ['bgColor' => '#eee']
            );

            $table = $section->addTable('Purchaser Table');
            $table->addRow(900);
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('Name of Purchaser');
            $table->addCell(2000)->addText('Lumpsum');
            $table->addCell(2000)->addText('Stamp and legal fees');
            $table->addCell(2000)->addText('Mthly instalment %');
            $table->addCell(2000)->addText('Signed by way of confirmation');

            $table->addRow();
            $table->addCell(2000)->addText('1');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');

            $table->addRow();
            $table->addCell(2000)->addText('2');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');

            $table->addRow();
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('Total Amount');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');
            $table->addCell(2000)->addText('');
        };

        $this->docMerge($template, $docVars, [
            'callbacks' => $callbacks,
            'exportName' => 'Unit pre calculation'
        ]);
    }

    public function letterOfAuth($id = null)
    {
        // $userId = $this->request->session()->read('Auth.User.id');
        // temporary for demo
        $userId = 1;
        $template = $this->SettingsRepo->getDocumentTemplates($userId, 'letter_of_authority_client_property');
        $docVars = [];

        $this->docMerge($template, $docVars, [
            'exportName' => 'Letter of Authority'
        ]);
    }

    private function docMerge($template, $docVars, $options = [])
    {
        $defaultOptions = [
            'exportName' => 'Export',
            'callbacks' => null
        ];

        $templateOptions = (object)array_merge($defaultOptions, $options);

        if ($template->template_soure == 'file') {
            // TODO: Make the org 1 dynamic or check current user
            // TODO: Move this path generation to util file
            $fileDir = implode(DS, [
                dirname(ROOT), 'uploads', 'document_templates',
                'org1',
                ''
            ]);

            $filepath = $fileDir . $template->template_file;

            $this->fileMerge(
                $filepath,
                $docVars,
                $templateOptions->exportName,
                $templateOptions->callbacks
            );
        } else {
            $this->htmlMerge(
                $docVars,
                $template->template_html,
                $templateOptions->exportName,
                $templateOptions->callbacks
            );
        }
    }

    private function htmlMerge($docVars, $templateHtml, $exportFileName = 'Export', $callbacks = null)
    {
        foreach ($docVars as $varKey => $varVal) {
            $templateHtml = str_replace(sprintf('${%s}', $varKey), $varVal, $templateHtml);
        }

        try {
            $phpWord = new PhpWord();
            $section = $phpWord->addSection();

            // clean tags
            // dont wrap table inside a p
            $templateHtml = str_replace(
                ['<br>',  '<i>', '</i>', '<b>',     '</b>',     '<p><table>','</table></p>','&amp;'],
                ['<br />','<em>','</em>','<strong>','</strong>','<table>',   '</table>',    'and'],
                $templateHtml
            );
            $templateHtml = strip_tags($templateHtml, '<p><b><i><span><em><strong><br><h1><h2><h3><h4><h5><h6><ul><ol><li><table><tr><th><td><pre>');


            // inject html to word
            Html::addHtml($section, $templateHtml, false);

            // after html event
            if (is_array($callbacks) && is_callable($callbacks['after_html'])) {
                $callbacks['after_html']($phpWord, $section);
            }

            $tmpFilename = uniqid('compl-itm-') ;
            $docfile = dirname(dirname(APP)) . '/temp/docs/' . $tmpFilename . '.docx';
            $exportFileName = $exportFileName . '.docx';

            // save document temporarily
            $phpWord->save($docfile, 'Word2007');

            // echo to browser
            $fp = fopen($docfile, 'rb'); // open the file in a binary mode

            header("Content-Type: application/vnd.openxmlformats-package.relationships+xml"); // send the right headers
            header("Content-Length: " . filesize($docfile));
            header('Content-Disposition: attachment; filename="' . $exportFileName . '"');

            // dump the picture and stop the script
            fpassthru($fp);

            unlink($docfile);
        } catch (\Exception $e) {
            die('Unable to generate document');
            exit;
        }
    }

    public function fileMerge($filepath, $docVars = [], $exportFileName = 'Export', $callbacks = null)
    {
        if (!is_file($filepath)) {
            throw new \Exception('Unable to locate template file');
        }

        try {
            //dr($filepath);
            $template = new \PhpOffice\PhpWord\TemplateProcessor($filepath);

            foreach ($docVars as $docVarKey => $docVarVal) {
                $template->setValue($docVarKey, $docVarVal);
                $template->setValue($docVarKey . '_upper', strtoupper($docVarVal));
            }

            $tmpFilename = uniqid('compl-itm-') ;
            $docfile = dirname(dirname(APP)) . '/temp/docs/' . $tmpFilename . '.docx';
            $exportFileName = $exportFileName . '.docx';

            $template->saveAs($docfile);

            // echo to browser
            $fp = fopen($docfile, 'rb'); // open the file in a binary mode

            header("Content-Type: application/vnd.openxmlformats-package.relationships+xml"); // send the right headers
            header("Content-Length: " . filesize($docfile));
            header('Content-Disposition: attachment; filename="' . $exportFileName . '"');

            // dump the picture and stop the script
            fpassthru($fp);

            unlink($docfile);
            exit;
        } catch (Exception $e) {
            throw new \Exception('Unable to generate template file');
        }
    }
}
