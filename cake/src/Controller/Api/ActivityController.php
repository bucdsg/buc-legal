<?php

namespace App\Controller\Api;

use Cake\I18n\Time;

use App\Controller\AppController;
use App\Repo\ActivityRepo;

class ActivityController extends AppController
{
    protected $activityRepo;
    protected $dateFormat;

    public function initialize()
    {
        // parent::initialize();
        $this->activityRepo = new ActivityRepo;
        $this->dateFormat = 'd/m/Y H:i:s';
    }

    public function getUserSettings()
    {
        $userId = $this->request->session()->read('Auth.User.id');

        $response = $this->activityRepo->getUserSettings($userId);

        return $this->jsend($response);
    }

    public function getActivities()
    {
        try {
            $jsonData = $this->request->input('json_decode');

            if (!$jsonData) {
                throw new \Exception;
            }

            $data = (object)[
                'object' => $jsonData->object,
                'id' => $jsonData->id
            ];

            $result = $this->activityRepo->getActivities($data);

            return $this->jsend($result);
        } catch (\Exception $e) {
            return $this->jsend([], 'error', 'Error getting activities');
        }
    }

    public function saveActivity()
    {
        try {
            $jsonData = $this->request->input('json_decode');

            $data = (object)[
                'details' => $jsonData->details,
                'type' => $jsonData->type,
                'activityObject' => $jsonData->activityObject,
                'due_date' => $jsonData->dueDate
            ];

            if (!empty($data->due_date)) {
                $data->due_date = Time::createFromFormat(
                    $this->dateFormat,
                    $data->due_date . ' 00:00:00'
                );
            }

            $result = $this->activityRepo->saveActivity($data);

            return $this->jsend($data, $result, $this->activityRepo->getError());
        } catch (\Exception $e) {
            return $this->jsend([], 'error', 'Unable to save activity', $e->getMessage());
        }
    }
}
