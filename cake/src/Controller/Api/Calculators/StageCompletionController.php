<?php

namespace App\Controller\Api\Calculators;

use Cake\I18n\Time;

use App\Controller\AppController;
use App\Library\BUC\StageCompletionCalculator;

class StageCompletionController extends AppController
{
    protected $dateFormat;

    public function initialize()
    {
        // add this to disable user authentication error
        // parent::initialize();

        $this->dateFormat = 'd/m/Y H:i:s';
    }

    private function _parseDate($date) {
        if (!$date) {
            return null;
        }

        return Time::createFromFormat(
            $this->dateFormat,
            $date . ' 00:00:00'
        );
    }

    private function _parseData($data) {
        $required = [
            'purchasePrice',
            'completionDate',
            'cscDate'
        ];
        $dateInputs = [
            'completionDate',
            'cscDate'
        ];
        $optional = [];
        $data = (object)$data;
        $result = (object)[
            'purchasePrice' => null,
            'completionDate' => null,
            'cscDate' => null,
            'stages' => []
        ];

        foreach ($required as $item) {
            if (!$data || !$data->$item) {
                throw new \Exception('Missing ' . $item);
            }

            if (in_array($item, $dateInputs)) {
                $result->$item = $this->_parseDate($data->$item);
            } else {
                $result->$item = $data->$item;
            }
        }

        // get stages
        if ($data->stages && is_array($data->stages)) {
            foreach ($data->stages as $stage) {
                $result->stages[] = (object)[
                    'id' => $stage->id,
                    'dateNotice' => $this->_parseDate($stage->dateNotice),
                    'datePayment' => $this->_parseDate($stage->datePayment)
                ];
            }
        }

        return $result;
    }

    public function calculate()
    {
        $jsonData = $this->request->input('json_decode');

        try {
            if (!$jsonData) {
                throw new \Exception('Invalid input');
            }

            $data = $this->_parseData($jsonData);

            $calculator = new StageCompletionCalculator;
            $calculator->setData($data)->calculate();

            $result = [
                'data' => $data,
                'html' => $calculator->resultText()
            ];

            return $this->jsend($result);
        } catch (\Exception $e) {
            return $this->jsend(null, 'error', $e->getMessage());
        }
    }
}
