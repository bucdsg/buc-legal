<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use App\Repo\StatsRepo;

class DashboardController extends AppController
{
    protected $statsRepo;

    public function initialize()
    {
        $this->statsRepo = new StatsRepo;
    }

    public function stageSummaryByProjectId($id)
    {
        $result = $this->statsRepo->getStagesSummaryByProjectId($id);

        return $this->jsend($result);
    }
}
