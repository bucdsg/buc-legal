<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use App\Repo\ProjectRepo;

class ProjectController extends AppController
{
    protected $ProjectRepo;

    public function initialize()
    {
        $this->ProjectRepo = new ProjectRepo;
    }

    public function index()
    {
        $developerId = 1; // TODO: get current user id in session

        $response = $this->ProjectRepo->getByDeveloperId($developerId);

        return $this->jsend($response);
    }

    public function findById($id = null)
    {
        $response = $this->ProjectRepo->getById($id);

        if (empty($response)) {
            return $this->jsend(null, 'error', 'Not found');
        }

        return $this->jsend($response);
    }

    public function saveProjectDetails($id = null)
    {
        if (empty($id)) {
            return $this->jsend(null, 'error', 'Not found');
        }

        try {
            $this->ProjectRepo->saveById($id, $this->request->input('json_decode'));
        } catch (\Exception $e) {
            return $this->jsend(null, 'error', 'Incorrect Data');
        }

        return $this->findById($id);
    }

    /**
     * Create new project but will only fetch
     * project name and properties mapping
     */
    public function initializeNewProject()
    {
        try {
            $jsonData = $this->request->input('json_decode');

            $data = [
                'project_name' => $jsonData->name,
                'project_description' => $jsonData->description,
                'project_address' => $jsonData->address,
                'unit_mapping' => $jsonData->unitsMap,
                'developer_id' => 1
            ];

            $result = $this->ProjectRepo->createNewProject($data);

            return $this->jsend($data, $result, $this->ProjectRepo->getError());
        } catch (\Exception $e) {
            return $this->jsend([], 'error', 'Unable to save project');
        }
    }

    public function completionItems()
    {
        $response = $this->ProjectRepo->getCompletionTypes();

        return $this->jsend($response);
    }

    public function unitStageCompletions($projectId)
    {
        $response = $this->ProjectRepo->completionItemsByProperty($projectId);

        return $this->jsend($response);
    }

    public function newCompletionItem($projectId)
    {
        try {
            $jsonData = $this->request->input('json_decode');

            $data = [
                'completion_id' => $jsonData->completionId,
                'property_ids' => array_map(function($unit){
                    return $unit->id;
                }, $jsonData->units)
            ];

            $result = $this->ProjectRepo->newCompletionItem($projectId, $data);

            return $this->jsend($data, $result, $this->ProjectRepo->getError());
        } catch (\Exception $e) {
            return $this->jsend([], 'error', 'Unable to save data');
        }
    }
}
