<?php

namespace App\Controller\Api;

use Cake\I18n\Time;
use Cake\Mailer\Email;

use App\Controller\AppController;
use App\Repo\MilestoneRepo;
use App\Repo\NoticeRepo;

class MilestoneController extends AppController
{
    protected $milestoneRepo;
    protected $noticeRepo;

    public function initialize()
    {
        $this->dateFormat = 'd/m/Y H:i:s';
        $this->milestoneRepo = new MilestoneRepo;
        $this->noticeRepo = new NoticeRepo;
    }

    public function setUnitCompletionDate()
    {
        $dateFormat = 'd/m/Y H:i:s';
        $timezone = 'America/New_York';

        try {
            $jsonData = $this->request->input('json_decode');

            $completedDate = Time::createFromFormat(
                $dateFormat,
                $jsonData->completed_at . ' 00:00:00'
            );

            $scheduleDate = null;

            if ($jsonData->scheduled_at) {
                $scheduleDate = Time::createFromFormat(
                    $dateFormat,
                    $jsonData->scheduled_at . ' 00:00:00'
                );
            }

            if ($completionId = $jsonData->id) {
                // get the completion data
                $completionItem = $this->milestoneRepo->getByCompletionId($completionId);

                if ($completionItem) {
                    $completionItem->completed_at = $completedDate;
                    $completionItem->scheduled_at = $scheduleDate;

                    $this->milestoneRepo->updateCompletion($completionItem);
                }
            } else {
                // create new entry
                $result = $this->milestoneRepo->newCompletionDate(
                    $jsonData->property_id,
                    $jsonData->completionTypeId,
                    $completedDate,
                    $scheduleDate
                );

                if (!$result) {
                    return $this->jsend([], 'error', 'Unable to generate stage completion');
                }

                // create new notice item
                $notice = $this->noticeRepo->newStageCompletionNotice([
                    'property_id' => $jsonData->property_id,
                    'recipient_entity' => 'client',
                    'recipient_type' => 'email',
                    'stage_completion' => $result
                ]);

                return $this->jsend($result);
            }

            return $this->jsend($jsonData);

        } catch (\Exception $e) {
             return $this->jsend([], 'error', 'Unable to save project. ' . $e->getMessage());
        }
    }

    public function setUnitCompletionDateMultiple()
    {
        $dateFormat = 'd/m/Y';

        try {
            $jsonData = $this->request->input('json_decode');

            $dates = [
                'letter_date' => Time::createFromFormat($dateFormat, $jsonData->letter_date),
                'letter_received_date' => Time::createFromFormat($dateFormat, $jsonData->date_received),
                'payment_action_date' => Time::createFromFormat($dateFormat, $jsonData->payment_action_date),
                'payment_due_date' => Time::createFromFormat($dateFormat, $jsonData->payment_due_date),
                'due_date_to_developer' => Time::createFromFormat($dateFormat, $jsonData->due_date_to_developer)
            ];
            $stageUnits = $jsonData->stageUnits;
            $stageUnitsFormatted = [];
            $whereCond = [];

            if (is_array($jsonData->stageUnits)) {
                foreach ($jsonData->stageUnits as $stageUnit) {
                    foreach ($stageUnit->units as $sUnit) {
                        $stageUnitsFormatted[] = (object)array_merge($dates, [
                            'completion_type_id' => $stageUnit->stageId,
                            'property_id' => $sUnit->id,
                        ]);

                        $whereCond[] = (object) [
                            'property_id' => $sUnit->id,
                            'completion_type_id' => $stageUnit->stageId
                        ];
                    }
                }
            }

            $this->milestoneRepo->upsertMultiple($stageUnitsFormatted);

            return $this->jsend($jsonData);

        } catch (\Exception $e) {
            return $this->jsend([], 'error', 'Unable to save completion items. ' . $e->getMessage());
        }
    }

    public function getUnpaidStages()
    {
        return $this->jsend($this->milestoneRepo->getUnpaid());
    }

    private function sendNoticeEmail($notice)
    {
        $email = new Email('mailgun');
        $email->template('stagecompletion');
        $email->emailFormat('both');
        $email->from('no-reply@buc.local');
        $email->to('kevinfelisilda@gmail.com', 'Kevin');
        $email->subject('PURCHASE OF HENNAN RESORT 7-03 THE HILLER SINGAPORE 6677978');
        //$email->viewVars(['url' => $url, 'username' => $user->username]);
        if ($email->send()) {
            // $this->Flash->success(__('Check your email for your reset password link'));
        } else {
            // $this->Flash->error(__('Error sending email: ') . $email->smtpError);
        }
        $this->noticeRepo->setlastSent($notice->id, new Time());
    }

    public function sendNotice($noticeId)
    {
        $notice = $this->noticeRepo->getById($noticeId);

        if (empty($notice)) {
            return $this->jsend([], 'error', 'Invalid Id');
        }

        // if resolved no need to send
        if ($notice->resolved_at) {
            return $this->jsend([], 'error', 'Already resolved');
        }

        $this->sendNoticeEmail($notice);

        return $this->jsend([]);
    }

    public function resendNotice($noticeId)
    {
        $notice = $this->noticeRepo->getById($noticeId);

        if (empty($notice)) {
            return $this->jsend([], 'error', 'Invalid Id');
        }

        // if resolved no need to send
        if ($notice->resolved_at) {
            return $this->jsend([], 'error', 'Already resolved');
        }

        $this->sendNoticeEmail($notice);

        return $this->jsend([]);
    }

    public function markResolve($noticeId)
    {
        $notice = $this->noticeRepo->getById($noticeId);

        if (empty($notice)) {
            return $this->jsend([], 'error', 'Invalid Id');
        }

        // if resolved no need to send
        if ($notice->resolved_at) {
            return $this->jsend([], 'error', 'Already resolved');
        }

        $this->noticeRepo->markResolve($notice->id);

        return $this->jsend([]);
    }

    public function setStagePaymentDate()
    {
        try {
            $jsonData = $this->request->input('json_decode');

            $this->milestoneRepo->setStagePaymentDate(
                $jsonData->completionId,
                Time::createFromFormat(
                    $this->dateFormat,
                    $jsonData->paymentDate . ' 00:00:00'
                )
            );

            return $this->jsend($jsonData);
        } catch (\Exception $e) {
            return $this->jsend([], 'error', 'Unable to set payment date. ' . $e->getMessage());
        }
    }
}
