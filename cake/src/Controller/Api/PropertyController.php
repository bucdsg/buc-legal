<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use App\Repo\PropertyRepo;

class PropertyController extends AppController
{
    protected $PropertyRepo;

    public function initialize()
    {
        $this->PropertyRepo = new PropertyRepo;
    }

    public function findById($id = null)
    {
        $response = $this->PropertyRepo->getById($id);

        if (empty($response)) {
            return $this->jsend(null, 'error', 'Not found');
        }

        return $this->jsend($response);
    }

    /**
     * Create new project but will only fetch
     * project name and properties mapping
     */
    public function assignNewClient($propertyId)
    {
        try {
            $jsonData = $this->request->input('json_decode');

            $data = [
                'name' => $jsonData->clientName,
                'address' => $jsonData->clientAddress,
                'purchase_amount' => $jsonData->purchasePrice,
                'cash_amount' => $jsonData->cashAmount,
                'loan_amount' => $jsonData->loanAmount,
                'cpf1_name' => $jsonData->cpf1Name,
                'cpf2_name' => $jsonData->cpf2Name,
                'cpf3_name' => $jsonData->cpf3Name,
                'cpf1_amount' => $jsonData->cpf1Amount,
                'cpf2_amount' => $jsonData->cpf2Amount,
                'cpf3_amount' => $jsonData->cpf3Amount,
                'stages' => $jsonData->stages
            ];

            $result = $this->PropertyRepo->assignNewClient($propertyId, $data);

            return $this->jsend($data, $result);
        } catch (\Exception $e) {
            return $this->jsend([], 'error', 'Unable to save project');
        }
    }
}
