<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use App\Repo\SettingsRepo;

class SettingsController extends AppController
{
    protected $settingsRepo;

    public function initialize()
    {
        // parent::initialize();
        $this->settingsRepo = new SettingsRepo;
    }

    public function getUserSettings()
    {
        $userId = $this->request->session()->read('Auth.User.id');

        $response = $this->settingsRepo->getUserSettings($userId);

        return $this->jsend($response);
    }

    public function saveUserSettings()
    {
        $userId = $this->request->session()->read('Auth.User.id');

        if (empty($userId)) {
            return $this->jsend([], 'error', 'Unable to save data');
        }

        try {
            $jsonData = $this->request->input('json_decode');
            $this->settingsRepo->saveUserSettings($userId, $jsonData);

            return $this->jsend($jsonData);
        } catch (\Exception $e) {
            return $this->jsend([], 'error', 'Unable to save data. ' . $e->getMessage());
        }
    }

    public function documentTemplate()
    {
        // $userId = $this->request->session()->read('Auth.User.id');
        // temporary
        $userId = 1;

        if (empty($userId)) {
            return $this->jsend([], 'error', 'Authentication Error');
        }

        try {
            $jsonData = $this->request->input('json_decode');
            if (!empty($jsonData->templates)) {
                $this->settingsRepo->setDocumentTemplates($userId, $jsonData->templates);
            }
        } catch (\Exception $e) {
            return $this->jsend([], 'error', 'Error: ' . $e->getMessage());
        }

        return $this->jsend($this->settingsRepo->getDocumentTemplates($userId));
    }
}
