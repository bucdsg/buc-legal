<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use App\Repo\ClientRepo;

class ClientController extends AppController
{
    protected $ClientRepo;

    public function initialize()
    {
        // parent::initialize();
        $this->ClientRepo = new ClientRepo;
    }

    public function index()
    {
        $response = $this->ClientRepo->getClients();

        return $this->jsend($response);
    }

    public function view($id = null)
    {
        $response = $this->ClientRepo->getById($id);

        if (empty($response)) {
            return $this->jsend(null, 'error', 'Not found');
        }

        return $this->jsend($response);
    }


  /*
    public function findById($id = null)
    {
        $response = $this->ClientRepo->getById($id);

        if (empty($response)) {
            return $this->jsend(null, 'error', 'Not found');
        }

        return $this->jsend($response);
    }
    */

    public function edit($id)
    {
        try {

            if ($this->request->is(['post', 'put'])) {
                $response = $this->ClientRepo->edit($id, $this->request->getData());
            }

            return $this->jsend($response);

         } catch (\Exception $e) {
            return $this->jsend([], 'error', 'Unable to save client');
        }
    }



}
