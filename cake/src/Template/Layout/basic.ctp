<?php
use Cake\Routing\Router;

$baseUrl = Router::url('/', true);
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">

  <title>Forgot password | Remark Admin Template</title>

  <link rel="apple-touch-icon" href="../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../assets/images/favicon.ico">

  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,300italic">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="<?= $baseUrl ?>global/css/bootstrap.min.css?e08c5cea">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/css/bootstrap-extend.min.css?e08c5cea">



  <link rel="stylesheet" href="<?= $baseUrl ?>css/pages/forgot-password.min.css">

  <link rel="stylesheet" href="<?= $baseUrl ?>global/vendor/flag-icon-css/flag-icon.css?e08c5cea">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/fonts/glyphicons/glyphicons.min.css?e08c5cea">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/fonts/web-icons/web-icons.min.css?e08c5cea">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/fonts/brand-icons/brand-icons.min.css?e08c5cea">

  <link rel="stylesheet" href="<?= $baseUrl ?>css/site.min.css?e08c5cea">



  <!-- Fonts -->

  <link rel="stylesheet" href="<?= $baseUrl ?>global/vendor/flag-icon-css/flag-icon.css?e08c5cea">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/fonts/glyphicons/glyphicons.min.css?e08c5cea">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/fonts/web-icons/web-icons.min.css?e08c5cea">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/fonts/brand-icons/brand-icons.min.css?e08c5cea">

  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="<?= $baseUrl ?>global/vendor/breakpoints/breakpoints.js"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body class="page-forgot-password layout-full">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


  <!-- Page -->
  <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
  data-animsition-out="fade-out">
   <?= $this->fetch('content') ?>

  </div>
  <!-- End Page -->

 <script src="<?= $baseUrl ?>global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/jquery/jquery.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/tether/tether.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/bootstrap/bootstrap.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>

  <script src="<?= $baseUrl ?>global/vendor/screenfull/screenfull.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/slidepanel/jquery-slidePanel.js"></script>

  <script src="<?= $baseUrl ?>global/js/Plugin/animsition.js"></script>

  <script src="<?= $baseUrl ?>global/js/State.js"></script>
  <script src="<?= $baseUrl ?>global/js/Component.js"></script>
  <script src="<?= $baseUrl ?>global/js/Plugin.js"></script>
  <script src="<?= $baseUrl ?>global/js/Base.js"></script>
  <script src="<?= $baseUrl ?>js/Section/Menubar.js"></script>
  <script src="<?= $baseUrl ?>js/Section/Sidebar.js"></script>
  <script src="<?= $baseUrl ?>js/Plugin/menu.js"></script>
  <script src="<?= $baseUrl ?>js/Site.min.js"></script>

  <script src="<?= $baseUrl ?>app/bundle.js?v=<?php echo rand(9999, 99999) ?>"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
</body>

</html>
