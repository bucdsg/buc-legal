<div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">
  <div class="page-content">
    <div class="page-brand-info">
      <div class="brand">
        <h2 class="brand-text font-size-40">Digital BUC Platform</h2>
      </div>
      <p class="font-size-20">Digital OTP, S&amp;P and Progress Payment delivery</p>
    </div>
    <div class="page-login-main animation-slide-right animation-duration-1">
      <div class="brand xhidden-md-up">
        <!-- <img class="brand-img" src="http://flexiwriter.local/img/logo-square.png?1497763139" alt="..."> -->
        <h3 class="brand-text font-size-40">Digital BUC</h3>
      </div>
      <h3 class="font-size-24">Sign In</h3>
      <p>Login to access your account.</p>
      <?= $this->Flash->render() ?>
      <?php if(isset($auth_error)):?>
          <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
             <?= $this->Flash->render() ?>
          </div>
      <?php endif;?>

      <form method="post" accept-charset="utf-8">
        <div class="form-group">
          <label class="sr-only" for="inputEmail">Email</label>
          <?php echo $this->Form->control('email',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter your email address'));?>
        </div>
        <div class="form-group">
          <label class="sr-only" for="inputPassword">Password</label>
           <?php echo $this->Form->control('password',array('label'=>false,'class'=>'form-control','placeholder'=>'Enter your password'));?>

        </div>
        <div class="form-group clearfix">
          <div class="checkbox-custom checkbox-inline checkbox-primary float-left">
            <input type="checkbox" id="rememberMe" name="rememberMe">
            <label for="rememberMe">Remember me</label>
          </div>
          <a class="float-right" href="forgot_password">Forgot password?</a>
        </div>
        <?php echo $this->Form->button('Sign in',array('class'=>'btn btn-primary btn-block'));?>
      </form>
      <!-- p>No account? <a href="register-v2.html">Sign Up</a></p -->
      <footer class="page-copyright">
        <p>Powered by Amicus</p>
        <p>© <?php echo date('Y');?>. All RIGHT RESERVED.</p>
      </footer>
    </div>
  </div>
</div>
