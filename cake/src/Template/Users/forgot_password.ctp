
<div class="page-content vertical-align-middle">
  <h2>Reset Password</h2>
  <?php echo $this->Form->create() ?>
    <div class="form-group">
      <?php
        echo $this->Form->input('email', ['class'=>'form-control','required' => true, 'autofocus' => true,'placeholder'=>'Your Email','label'=>false]); ?>
       
    </div>

 
    <div class="form-group">
        <?php echo $this->Form->button(__('Reset Your Password'),['class'=>'btn btn-primary btn-block']); ?>
    </div>
   <?php echo $this->Form->end(); ?>

 
</div>
