<?php
use Cake\Routing\Router;

$baseUrl = Router::url('/', true);
?><!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <title>BUC</title>

  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,500,300italic">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="<?= $baseUrl ?>global/css/bootstrap.min.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/css/bootstrap3-4-fix.css?5dfb46fd">

  <link rel="stylesheet" href="<?= $baseUrl ?>global/vendor/animsition/animsition.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/vendor/asscrollable/asScrollable.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/vendor/switchery/switchery.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/vendor/slidepanel/slidePanel.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/vendor/chartist/chartist.css">

  <link rel="stylesheet" href="<?= $baseUrl ?>css/apps/work.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>css/pages/user.min.css?f2fa460">

  <link rel="stylesheet" href="<?= $baseUrl ?>global/vendor/flag-icon-css/flag-icon.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/fonts/glyphicons/glyphicons.min.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/fonts/web-icons/web-icons.min.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/fonts/brand-icons/brand-icons.min.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>global/fonts/font-awesome/font-awesome.min.css?f2fa460">

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.5/sweetalert2.min.css">

  <link rel="stylesheet" href="<?= $baseUrl ?>css/site.min.css?f2fa460">
  <link rel="stylesheet" href="<?= $baseUrl ?>css/buc.css?9fdf1d5">

  <style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {display: none !important;}
  </style>
  <script src="<?= $baseUrl ?>global/vendor/breakpoints/breakpoints.js"></script>
  <script type="text/javascript">window.__baseUrl = '<?= $baseUrl ?>'</script>
  <script>
    Breakpoints();
  </script>
</head>
<body ng-app="bucApp" class="site-navbar-small app-work-x page-user">
  <div header></div>
  <div class="page">
    <page-spinner></page-spinner>
    <div ui-view autoscroll="true"></div>
  </div>
  <div footer></div>
  <script src="<?= $baseUrl ?>global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/jquery/jquery.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/tether/tether.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/bootstrap/bootstrap.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/chartist/chartist.min.js"></script>

  <script src="<?= $baseUrl ?>global/vendor/screenfull/screenfull.js"></script>
  <script src="<?= $baseUrl ?>global/vendor/slidepanel/jquery-slidePanel.js"></script>

  <script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.5/sweetalert2.min.js"></script>
  <script src="<?= $baseUrl ?>global/js/Plugin/animsition.js"></script>

  <script src="<?= $baseUrl ?>global/js/State.js"></script>
  <script src="<?= $baseUrl ?>global/js/Component.js"></script>
  <script src="<?= $baseUrl ?>global/js/Plugin.js"></script>
  <script src="<?= $baseUrl ?>global/js/Base.js"></script>
  <script src="<?= $baseUrl ?>js/Section/Menubar.js"></script>
  <script src="<?= $baseUrl ?>js/Section/Sidebar.js"></script>
  <script src="<?= $baseUrl ?>js/Plugin/menu.js"></script>
  <script src="<?= $baseUrl ?>js/Site.min.js"></script>

  <script src="<?= $baseUrl ?>app/bundle.js?v=<?php echo rand(9999, 99999) ?>"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
</body>
</html>
