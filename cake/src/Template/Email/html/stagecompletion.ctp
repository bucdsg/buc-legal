Dear Sir/Mdm,
<br>
<br>
<p><b>PURCHASE OF HENNAN RESORT 7-03 THE HILLER SINGAPORE 6677978</b></p>
<br>
<p>We refer to the above matter.</p>

<p>We would like to inform you that the progress payment under clause 5.1.2(a) of the Sale and Purchase Agreement is due to the Developers on 9/1/17, 12:00 AM.</p>

<p>We enclose herewith a copy of the developer's solicitors' letter dated 9/1/17, 12:00 AM together with the architect's certificate for your attention.</p>

<p>In the premises, kindly let us have a cheque for <b>$10,000.00</b> issued in favor of <b>"OCBC FOR PROJECT ACCOUNT NO. 629-866997-001 OF TRANSURBAN PROPERTIES PTE LTD"</b> by <b>9/1/17, 12:00 AM</b>.</p>
<br>
<br>
<br>
<br>
Yours faithfully,
<br>
<br>
<b>Kevin Felisilda</b>
<br>
Director
<br>
<br>
<br>
Assistant. Cassandra
<br>
DID: 6866 0335
<br>
Direct Fax: 6337 0906
<br>
Email: cassandra@example.com
