<?php
namespace App\Library\BUC;

use Cake\I18n\Time;

class StageCompletionCalculator {

    protected $stages;
    protected $purchasePrice;
    protected $completionDate;
    protected $cscDate;

    public function __construct($data = null)
    {
        $this->stages = (object)[(object)[
            'id' => 100,
            'payment_stage_id' => '1',
            'name' => 'upon the grant of option to purchase',
            'percentage' => 0.05,
            'amount' => 0,
            'dateNotice' => null,
            'datePayment' => null,
            'notes' => ''
        ], (object)[
            'id' => 101,
            'payment_stage_id' => '1',
            'name' => 'upon signing S & P',
            'percentage' => 0.15,
            'amount' => 0,
            'dateNotice' => null,
            'datePayment' => null,
            'notes' => ''
        ], (object)[
            'id' => 1,
            'payment_stage_id' => '2.a',
            'name' => 'completion of foundation work',
            'percentage' => 0.10,
            'amount' => 0,
            'dateNotice' => null,
            'datePayment' => null,
            'notes' => ''
        ], (object)[
            'id' => 2,
            'payment_stage_id' => '2.b',
            'name' => 'completion of reinforced concrete framework of unit',
            'percentage' => 0.10,
            'amount' => 0,
            'dateNotice' => null,
            'datePayment' => null,
            'notes' => ''
        ], (object)[
            'id' => 3,
            'payment_stage_id' => '2.c',
            'name' => 'completion of brick walls of unit',
            'percentage' => 0.05,
            'amount' => 0,
            'dateNotice' => null,
            'datePayment' => null,
            'notes' => ''
        ], (object)[
            'id' => 4,
            'payment_stage_id' => '2.d',
            'name' => ' completion of roofing/ceiling of unit',
            'percentage' => 0.05,
            'amount' => 0,
            'dateNotice' => null,
            'datePayment' => null,
            'notes' => ''
        ], (object)[
            'id' => 5,
            'payment_stage_id' => '2.e',
            'name' => 'completion of electrical wiring, internal plastering, plumbing and instalation of door and window frames of unit',
            'percentage' => 0.05,
            'amount' => 0,
            'dateNotice' => null,
            'datePayment' => null,
            'notes' => ''
        ], (object)[
            'id' => 6,
            'payment_stage_id' => '2.f',
            'name' => 'completion of car park, roads and drains serving the housing project',
            'percentage' => 0.05,
            'amount' => 0,
            'dateNotice' => null,
            'datePayment' => null,
            'notes' => ''
        ], (object)[
            'id' => 7,
            'payment_stage_id' => '3',
            'name' => 'note of vacant possession',
            'percentage' => 0.25,
            'amount' => 0,
            'dateNotice' => null,
            'datePayment' => null,
            'notes' => ''
        ], (object)[
            'id' => 8,
            'payment_stage_id' => '4 or 5',
            'name' => ' on completion date',
            'percentage' => 0.15,
            'amount' => 0,
            'dateNotice' => null,
            'datePayment' => null,
            'notes' => ''
        ]];

        if ($data) {
            $this->setData($data);
        }
    }

    public function percentageFromPurchasePrice($percentage) {
        return number_format($this->purchasePrice * $percentage, 2);
    }

    public function setData($data)
    {
        $this->purchasePrice = $data->purchasePrice;
        $this->completionDate = $data->completionDate;
        $this->cscDate = $data->cscDate;

        // merge stages data
        if ($data->stages && is_array($data->stages)) {
            foreach ($this->stages as &$stage) {
                foreach ($data->stages as $dataStage) {
                    if ($stage->id == $dataStage->id) {
                        $stage->dateNotice = $dataStage->dateNotice;
                        $stage->datePayment = $dataStage->datePayment;
                        break;
                    }
                }
            }
        }

        return $this;
    }

    public function calculate()
    {
        $lastStage = end($this->stages);
        $finalPaymentDate = $lastStage ? $lastStage->datePayment : null;

        foreach ($this->stages as &$stage) {
            $stage->amount = $this->purchasePrice * $stage->percentage;
            $amount = number_format($stage->amount, 2);
            $percentage = number_format($stage->percentage * 100);
            $stage->notes = '';

            // for last payment step
            if ($stage->id == 8) {
                // project on schedule
                if ($this->completionDate->lte($this->cscDate)) {
                    // do item 4
                    if (!$finalPaymentDate || $finalPaymentDate->gt($this->completionDate)) {
                        // 4.a
                        // on completion date, pay:
                        // 2% purchase price to vendor/developer
                        // 13% to the SAL
                        $stage->payment_stage_id = '4.a';
                        $toVendor = $this->percentageFromPurchasePrice(0.02);
                        $toSAL = $this->percentageFromPurchasePrice(0.13);
                        $salToVendor= $this->percentageFromPurchasePrice(0.08);
                        $salToVendorWithDeductions = $this->percentageFromPurchasePrice(0.05);

                        $stage->notes .= <<<EOL
Completion date: $this->completionDate
Final payment date: $finalPaymentDate
On completion date
 - Pay $$toVendor (2%) to vendor/developer
 - Pay $$toSAL (13%) to SAL

>> Other note
SAL Receives vendor notification of CSC,
By last payment date
 - SAL pays $$salToVendor (8%) to vendor/developer
 - SAL pays $$salToVendorWithDeductions (5% - deductions) to vendor/developer
EOL;
                    } else {
                        // do 4.b
                        $stage->payment_stage_id = '4.b';
                        $hasDeductions = false;

                        if ($hasDeductions) {
                            // to be implemented
                        } else {
                        $toVendor = $this->percentageFromPurchasePrice(0.05);
                        $toSAL = $this->percentageFromPurchasePrice(0.08);
                        $toVendor2 = $this->percentageFromPurchasePrice(0.02);
                        $stage->notes .= <<<EOL
Completion date: $this->completionDate
Final payment date: $finalPaymentDate
On last payment date
- Pay $$toVendor (5%) to vendor/developer
On completion date (10%)
- Pay $$toVendor2 (2%) to vendor/developer
- Pay $$toSAL (8%) to SAL
EOL;
                        }
                    }
                } else {
                    // do item 5
                    if (!$finalPaymentDate || $finalPaymentDate->gt($this->cscDate)) {
                        // do 5.a
                        $stage->payment_stage_id = '5.a';
                        $cscNoticeReceived = $this->cscDate;
                        $lastPaymentDate = $cscNoticeReceived->addDays(14);

                        $toVendor = $this->percentageFromPurchasePrice(0.08);
                        $toVendorAfterCompletion = $this->percentageFromPurchasePrice(0.02);
                        $toSAL = $this->percentageFromPurchasePrice(0.05);
                        $salToVendor = $this->percentageFromPurchasePrice(0.05);

                        $stage->notes .= <<<EOL
Pay 13% of purchase price to
- $$toVendor (8%) to vendor/developer
- $$toSAL (5%) to SAL
On completion date
- $$toVendorAfterCompletion (2%) to vendor/developer

>> Other notes
- SAL pays $$salToVendor (5% - deductions) to vendor/developer
EOL;
                    } else {
                        // do 5.b
                        $stage->payment_stage_id = '5.b';
                        $cscNoticeReceived = $this->cscDate;
                        $lastPaymentDate = $cscNoticeReceived->addDays(14);

                        // no deductions for now
                        $deductions = false;

                        if ($deductions) {
                            // to be implemented
                        } else {
                            $toVendor1 = $this->percentageFromPurchasePrice(0.05);
                            $toVendor2 = $this->percentageFromPurchasePrice(0.08);
                            $toVendorAfterCompletion = $this->percentageFromPurchasePrice(0.02);
                            $stage->notes .= <<<EOL
On last payment date
- Pay $$toVendor1 (5%) to vendor/developer
Within (CSC Date + 14 Days)
- Pay $$toVendor2 (8%) to vendor/developer
On completion date
- Pay $$toVendorAfterCompletion (2%) to vendor/developer
EOL;
                        }
                    }
                }
            } else {
                $stage->notes = "Pay $$amount ($percentage%) to vendor/developer";

                if ($stage->dateNotice) {
                    $dateNoticeFormat = $stage->dateNotice->toDateString();
                    $datePaymentFormat = $stage->datePayment
                        ? $stage->datePayment->toDateString()
                        : 'None';
                    $stage->notes .= PHP_EOL;
                    $stage->notes .= "Notice Date $dateNoticeFormat";
                    $stage->notes .= PHP_EOL;
                    $stage->notes .= "Payment Date $datePaymentFormat";

                    $paymentDate = $stage->datePayment
                        ? $stage->datePayment
                        : Time::now();

                    $dateDue = $stage->dateNotice;
                    $dateDue->addDays(14);

                    if ($paymentDate->gte($dateDue)) {
                        $stage->notes .= PHP_EOL;
                        $stage->notes .= '[!] Stage should be paid within 14days after notice date';
                        $stage->notes .= PHP_EOL;
                        $stage->notes .= '[!] Subject to contract termination';
                    } else if (!$stage->datePayment) {
                        $stage->notes .= PHP_EOL;
                        $stage->notes .= 'Status: Awaiting payment';
                    }
                }
            }
        }
    }

    public function resultText()
    {
        $stagesContent = '';
        foreach ($this->stages as $stage) {
            $percentage = number_format($stage->percentage * 100);
            $amount = number_format($stage->amount, 2);
            $stageName = $stage->id < 100 ? $stage->id : $stage->name;

            $stagesContent .= <<<EOT
Stage: $stageName
Payment Stage: ($stage->payment_stage_id)
Percentage: $percentage%
Amount: $$amount
Notes:
$stage->notes


EOT;
        }

        return implode('<br />', [$stagesContent]);
    }
}
