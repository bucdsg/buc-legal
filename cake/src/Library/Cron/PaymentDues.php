<?php

namespace App\Library\Cron;

use Cake\I18n\Time;
use Mailgun\Mailgun;

use App\Repo\SysConfigRepo;
use App\Repo\PropertyCompletionRepo;
use App\Repo\NotificationRepo;

class PaymentDues {
    protected $result;
    protected $noticeRepo;

    public function __construct() {
        $this->noticeRepo = new NotificationRepo();
    }

    public function process() {
        $configRepo = new SysConfigRepo();
        $propertyCompletion = new PropertyCompletionRepo();

        try {
            // process notifications for payment due date
            $lastRun = $configRepo
                ->getConfig('cron_payment_due_last_run');

            $this->result .= PHP_EOL .
                ">> getting record from last run ". json_encode($lastRun);

            $items = $propertyCompletion
                ->StagesForPaymentNotification($lastRun);

            $this->result .= PHP_EOL .
                ">> found " . count($items) . " item(s)";

            $mg = Mailgun::create('key-2f9a520750caf3b184edef247fcc19ad');
            $mgDomain = 'sandbox79ab804cf61047f99d6fac1194f2bc3d.mailgun.org';
            $from = 'postmaster@sandbox79ab804cf61047f99d6fac1194f2bc3d.mailgun.org';

            foreach ($items as $item) {
                try {
                    if (!$this->notificationNeeded($item)) {
                        $this->result .= PHP_EOL .
                            ">> skip: stage_comp_id $item->id";
                        continue;
                    }

                    $parsed = $this->parseCompletionItem($item);
                    $emailBody = $this->generateEmailBody($parsed);
                    $jsonData = json_encode($parsed);

                    // send the email
                    // TODO: get actual email from user
                    $mg->messages()->send($mgDomain, [
                        'from'    => $from ,
                        'to'      => 'kevinfelisilda@gmail.com',
                        'subject' => 'Client Payment Due Reminder',
                        'text'    => $emailBody
                    ]);

                    $this->noticeRepo->save([
                        'company_id' => '1',
                        'type' => 'stage_completion',
                        'subtype' => 'payment_due',
                        'source' => 'cron_job',
                        'object_name' => 'property_completions',
                        'sent_via' => 'email',
                        'object_id' => $item->id,
                        'sent_at' => Time::now(),
                        'json_content' => $jsonData
                    ]);

                    $this->result .= PHP_EOL .
                        ">> ok: stage_comp_id $item->id email sent $jsonData";
                } catch (\Exception $e) {
                    // TODO: send an error alert notification

                    $errMessage = $e->getMessage();
                    $this->result .= PHP_EOL .
                        ">> err: stage_comp_id $item->id $errMessage";
                }
            }

            $configRepo->setConfig(
                'cron_payment_due_last_run',
                date('Y-m-d H:i:s')
            );
        } catch (\Exception $e) {
            $this->result = '> Cron err: ' . $e->getMessage();
        }
    }

    public function result() {
        return $this->result;
    }

    /**
     * Determine if this specific stage completion item
     * has already a notification sent.
     */
    private function notificationNeeded($item) {
        if (!$item || !$item->id) {
            return false;
        }

        $conds = [
            'type' => 'stage_completion',
            'subtype' => 'payment_due',
            'source' => 'cron_job',
            'object_id' => $item->id
        ];
        $notifications = $this->noticeRepo->getNotifications($conds);

        if ($notifications && count($notifications)) {
            return false;
        }

        return true;
    }

    private function parseCompletionItem($item) {
        if (!$item) {
            throw new \Exception('stage completion empty');
        }

        $totalAmount = 0;
        $paymentDistributions = [];

        if ($item && is_array($item->completion_payments)) {
            foreach ($item->completion_payments as $cp) {
                $totalAmount += $cp->amount;
                // add detailed data for payment distributions
                $paymentDistributions[] = (object)[
                    'amount' => $cp->amount,
                    'amountFormatted' => number_format($cp->amount, 2),
                    'modeOfPayment' => $cp->mode_of_payment
                ];
            }
        }

        if ($totalAmount <= 0) {
            throw new \Exception('total amount is empty');
        }

        if (empty($item->property) || empty($item->property->project)) {
            throw new \Exception('property or project not found');
        }

        if (empty($item->property->client)) {
            throw new \Exception('no assigned client found');
        }

        if (empty($item->property_completion_type)) {
            throw new \Exception('property_completion_type not found');
        }

        if (!$item->payment_due_date) {
            throw new \Exception('no payment due date detected');
        }

        $totalAmountFormatted = number_format($totalAmount, 2);
        $unitCompleteName = $item->property->name . ' ' . $item->property->project->name;
        $clientName = $item->property->client->name;
        $dueDate = $item->payment_due_date;
        $dueDateFormatted = $dueDate->toDateString();
        $stageNumber = $item->property_completion_type->id;
        $stageDescription = $item->property_completion_type->description;

        return (object)[
            'propertyId' => $item->property_id,
            'unitCompleteName' => $unitCompleteName,
            'stageNumber' => $stageNumber,
            'stageDescription' => $stageDescription,
            'clientName' => $clientName,
            'dueDate'=> $dueDate,
            'dueDateFormatted'=> $dueDateFormatted,
            'totalAmount' => $totalAmount,
            'totalAmountFormatted' => $totalAmountFormatted,
            'paymentDistributions' => $paymentDistributions,
            'notes' => $item->notes
        ];
    }

    private function generateEmailBody($parsed) {
        return <<<EOL
Reminder,

Client $parsed->clientName needs to pay SGD$parsed->totalAmountFormatted on or before $parsed->dueDateFormatted.
For the payment of ($parsed->stageNumber) $parsed->stageDescription.

Notes: $parsed->notes


BUC Legal Software


EOL;
    }
}
