<?php

namespace App\Shell;

use App\Library\Cron\PaymentDues;

use Cake\Console\Shell;

class CronShell extends Shell
{
    public function main()
    {
        $txt = <<<EOL
syntax: cron [command]
commands:
 payment_dues
EOL;
        $this->out($txt);
    }

    public function paymentDues()
    {
        $this->out('> cron: payment due start');

        $paymentDue = new PaymentDues();
        $paymentDue->process();
        $this->out($paymentDue->result());
    }
}
