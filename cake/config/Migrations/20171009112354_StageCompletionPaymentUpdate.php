<?php
use Migrations\AbstractMigration;

class StageCompletionPaymentUpdate extends AbstractMigration
{

    public function up()
    {

        $this->table('property_completion_payments')
            ->changeColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit' => null,
                'null' => true,
            ])
            ->update();

        $this->table('property_completions')
            ->changeColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit' => null,
                'null' => true,
            ])
            ->changeColumn('status', 'string', [
                'default' => 'created',
                'limit' => 50,
                'null' => false,
            ])
            ->update();

        $this->table('property_completions')
            ->addColumn('amount', 'decimal', [
                'after' => 'notes',
                'default' => '0.00',
                'null' => true,
                'precision' => 10,
                'scale' => 2,
            ])
            ->update();
    }

    public function down()
    {
        /*
        $this->table('property_completion_payments')
            ->changeColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'length' => null,
                'null' => false,
            ])
            ->update();

        $this->table('property_completions')
            ->changeColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'length' => null,
                'null' => false,
            ])
            ->changeColumn('status', 'string', [
                'comment' => 'status: pending, paid',
                'default' => 'pending',
                'length' => 50,
                'null' => false,
            ])
            ->removeColumn('amount')
            ->update();
        */
    }
}

