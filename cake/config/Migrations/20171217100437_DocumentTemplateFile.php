<?php
use Migrations\AbstractMigration;

class DocumentTemplateFile extends AbstractMigration
{

    public function up()
    {

        $this->table('document_templates')
            ->addColumn('template_soure', 'string', [
                'after' => 'template_html',
                'default' => 'html',
                'length' => 25,
                'null' => true,
            ])
            ->addColumn('template_file', 'text', [
                'after' => 'template_soure',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {
        /* $this->table('document_templates')
            ->removeColumn('template_soure')
            ->removeColumn('template_file')
            ->update(); */
    }
}

