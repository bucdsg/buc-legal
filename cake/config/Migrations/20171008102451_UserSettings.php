<?php
use Migrations\AbstractMigration;

class UserSettings extends AbstractMigration
{

    public $autoId = false;

    public function up()
    {

        $this->table('user_settings')
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
                'signed' => false,
            ])
            ->addColumn('key', 'string', [
                'default' => '',
                'limit' => 100,
                'null' => false,
            ])
            ->addPrimaryKey(['user_id', 'key'])
            ->addColumn('value', 'string', [
                'default' => '',
                'limit' => 500,
                'null' => true,
            ])
            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit' => null,
                'null' => true,
            ])
            ->create();
    }

    public function down()
    {
        /*
        $this->dropTable('user_settings');
        */
    }
}

