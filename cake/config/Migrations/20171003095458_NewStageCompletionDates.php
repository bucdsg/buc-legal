<?php
use Migrations\AbstractMigration;

class NewStageCompletionDates extends AbstractMigration
{

    public $autoId = false;

    public function up()
    {

        $this->table('property_completion_payments')
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('property_completion_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('mode_of_payment', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('amount', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('status', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('created_at', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('paid_at', 'timestamp', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('property_completions')
            ->addColumn('letter_date', 'timestamp', [
                'after' => 'created_at',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->addColumn('letter_received_date', 'timestamp', [
                'after' => 'letter_date',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->addColumn('payment_action_date', 'timestamp', [
                'after' => 'letter_received_date',
                'comment' => 'date to take action or remind client for payment if payment not complete',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->addColumn('payment_due_date', 'timestamp', [
                'after' => 'payment_action_date',
                'comment' => 'client payment due date',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->addColumn('due_date_to_developer', 'timestamp', [
                'after' => 'payment_due_date',
                'comment' => 'date to inform developer payment has been made',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {
        /* $this->table('property_completions')
            ->removeColumn('letter_date')
            ->removeColumn('letter_received_date')
            ->removeColumn('payment_action_date')
            ->removeColumn('payment_due_date')
            ->removeColumn('due_date_to_developer')
            ->update();

        $this->dropTable('property_completion_payments'); */
    }
}

