<?php
use Migrations\AbstractMigration;

class StageCompletionStatus extends AbstractMigration
{

    public function up()
    {

        $this->table('property_completions')
            ->addColumn('status', 'string', [
                'after' => 'due_date_to_developer',
                'comment' => 'status: pending, paid',
                'default' => 'pending',
                'length' => 50,
                'null' => false,
            ])
            ->update();
    }

    public function down()
    {
        /*
        $this->table('property_completions')
            ->removeColumn('status')
            ->update();
        */
    }
}

