<?php
use Migrations\AbstractMigration;

class ActivitiesDueDate extends AbstractMigration
{

    public function up()
    {

        $this->table('activities')
            ->addColumn('due_date', 'timestamp', [
                'after' => 'activity_text',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {
        /* $this->table('activities')
            ->removeColumn('due_date')
            ->update(); */
    }
}

