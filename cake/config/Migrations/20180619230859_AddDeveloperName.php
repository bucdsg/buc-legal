<?php
use Migrations\AbstractMigration;

class AddDeveloperName extends AbstractMigration
{

    public function up()
    {

        $this->table('projects')
            ->addColumn('developer_name', 'string', [
                'after' => 'updated',
                'default' => null,
                'length' => 100,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('projects')
            ->removeColumn('developer_name')
            ->update();
    }
}

