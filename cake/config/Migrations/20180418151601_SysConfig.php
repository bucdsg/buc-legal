<?php
use Migrations\AbstractMigration;

class SysConfig extends AbstractMigration
{

    public $autoId = false;

    public function up()
    {
        $this->table('sys_config')
            ->addColumn('config_key', 'string', [
                'limit' => 255,
                'null' => false
            ])
            ->addColumn('config_value', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addPrimaryKey(['config_key'])
            ->create();
    }

    public function down()
    {
        $this->dropTable('sys_config');
    }
}

