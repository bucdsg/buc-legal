<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'angular_index']);

    $routes->connect('/cake', ['controller' => 'Pages', 'action' => 'display', 'cakeinfo']);

    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('api', function ($routes) {
    $routes->connect('/project', ['controller' => 'Project', 'action' => 'index']);
    $routes->connect('/project/new/init', ['controller' => 'Project', 'action' => 'initializeNewProject']);
    $routes->connect('/project/:id', ['controller' => 'Project', 'action' => 'findById', '[method]' => 'GET'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/project/:id', ['controller' => 'Project', 'action' => 'saveProjectDetails', '[method]' => 'POST'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/completion-items', ['controller' => 'Project', 'action' => 'completionItems']);
    $routes->connect('/project/:id/completion-items/new', ['controller' => 'Project', 'action' => 'newCompletionItem'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/project/:id/properties/stage-completions', ['controller' => 'Project', 'action' => 'unitStageCompletions'], ['id' => '\d+', 'pass' => ['id']]);

    $routes->connect('/property/:id', ['controller' => 'Property', 'action' => 'findById'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/property/new/client/:id', ['controller' => 'Property', 'action' => 'assignNewClient'], ['id' => '\d+', 'pass' => ['id']]);

    $routes->connect('/set-completion-date', ['controller' => 'Milestone', 'action' => 'setUnitCompletionDate']);

    $routes->connect('/notice/:id/send', ['controller' => 'Milestone', 'action' => 'sendNotice'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/notice/:id/resend', ['controller' => 'Milestone', 'action' => 'resendNotice'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/notice/:id/mark-resolve', ['controller' => 'Milestone', 'action' => 'markResolve'], ['id' => '\d+', 'pass' => ['id']]);

    $routes->connect('/dashboard/stage-summary-by-project/:id', ['controller' => 'Dashboard', 'action' => 'stageSummaryByProjectId'], ['id' => '\d+', 'pass' => ['id']]);

    $routes->connect('/set-stage-completions', ['controller' => 'Milestone', 'action' => 'setUnitCompletionDateMultiple']);
    $routes->connect('/get-unpaid-stages', ['controller' => 'Milestone', 'action' => 'getUnpaidStages']);

    $routes->connect('/set-stage-payment-date', ['controller' => 'Milestone', 'action' => 'setStagePaymentDate']);

    $routes->connect('/get-user-settings', ['controller' => 'Settings', 'action' => 'getUserSettings']);
    $routes->connect('/save-user-settings', ['controller' => 'Settings', 'action' => 'saveUserSettings']);
    $routes->connect('/document-template', ['controller' => 'Settings', 'action' => 'documentTemplate']);

    $routes->connect('/save-activity', ['controller' => 'Activity', 'action' => 'saveActivity']);
    $routes->connect('/get-activities', ['controller' => 'Activity', 'action' => 'getActivities']);

    $routes->prefix('calculators', function ($routes) {
        $routes->connect('/stage-completion', [
            'controller' => 'StageCompletion',
            'action' => 'calculate'
        ]);
    });

    $routes->extensions(['json']);
    $routes->resources('Client');
});

Router::prefix('docs', function ($routes) {
    $routes->connect('/property/:id/milestone-list', ['controller' => 'PropertyDoc', 'action' => 'milestoneList'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/property/property-completion-item/:id', ['controller' => 'PropertyDoc', 'action' => 'stageCompletionNotice'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/property/notice-to-bank/:id', ['controller' => 'PropertyDoc', 'action' => 'noticeToBank'],  ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/property/notice-to-cpf/:id', ['controller' => 'PropertyDoc', 'action' => 'noticeToCPF'],  ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/property/:id/unit-pre-calculation', ['controller' => 'PropertyDoc', 'action' => 'unitPreCalculation'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/property/:id/letter-of-auth', ['controller' => 'PropertyDoc', 'action' => 'letterOfAuth'], ['id' => '\d+', 'pass' => ['id']]);

    $routes->connect('/upload', ['controller' => 'UploadDoc', 'action' => 'upload']);
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
