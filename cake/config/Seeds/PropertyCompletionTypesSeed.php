<?php
use Migrations\AbstractSeed;

/**
 * PropertyCompletionTypes seed.
 */
class PropertyCompletionTypesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'foundation_works',
                'description' => 'Foundation works of the unit',
                'created_at' => '2017-07-31 12:13:03',
                'updated_at' => NULL,
            ],
            [
                'id' => '2',
                'name' => 'reinforced_concrete_framework',
                'description' => 'Reinforced concrete framework of the unit',
                'created_at' => '2017-07-31 12:13:33',
                'updated_at' => NULL,
            ],
            [
                'id' => '3',
                'name' => 'brick_walls',
                'description' => 'Brick walls of the unit',
                'created_at' => '2017-07-31 12:14:21',
                'updated_at' => NULL,
            ],
            [
                'id' => '4',
                'name' => 'ceiling',
                'description' => 'Ceiling of unit',
                'created_at' => '2017-07-31 12:15:01',
                'updated_at' => NULL,
            ],
            [
                'id' => '5',
                'name' => 'door_window_electrical',
                'description' => 'Door & window frames in position, electrical wiring (without fittings), internal plastering and plumbing of the unit',
                'created_at' => '2017-07-31 12:15:31',
                'updated_at' => NULL,
            ],
            [
                'id' => '6',
                'name' => 'carpark_roads',
                'description' => 'Carpark, roads and drains serving the Housing Project',
                'created_at' => '2017-07-31 12:16:22',
                'updated_at' => NULL,
            ],
            [
                'id' => '7',
                'name' => 'building_roads_drainage_sewerage',
                'description' => 'The building and all roads, drainage and sewerage works in the Housing Project completed, and that water, electricity and gas supplies have been connected to building with TOP',
                'created_at' => '2017-07-31 12:15:31',
                'updated_at' => NULL,
            ],
            [
                'id' => '8',
                'name' => 'approval_authority',
                'description' => 'The approval from the competent Authority for the strate sub-divisional of the Project has been obtained',
                'created_at' => '2017-07-31 12:16:22',
                'updated_at' => NULL,
            ],
        ];

        $table = $this->table('property_completion_types');
        $table->insert($data)->save();
    }
}
