const webpack = require('webpack');
const path = require('path')

const config = {
  entry: {
    app: [
      'babel-polyfill',
      path.join(__dirname, 'angular', 'app.js')
    ]
  },
  output: {
    path: path.join(__dirname, 'public', 'app'),
    filename: 'bundle.js'
  },
  plugins: [],
  resolveLoader: {
    moduleExtensions: ['-loader']
  },
  module: {
    loaders: [
      {test: /\.js$/, loader: 'ng-annotate!babel', exclude: /node_modules/},
      {test: /\.html$/, loader: 'raw', exclude: /node_modules/},
      {test: /\.scss$/, loader: 'style!css!sass', exclude: /node_modules/},
      {test: /\.css$/, loader: 'style!css'},
      {test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'},
      {test: /\.(png|jpg)/, loader: 'file'},
    ]
  }
}

if (process.env.NODE_ENV === 'production') {
  config.plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = config;
