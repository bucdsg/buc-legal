#!/bin/bash

# install composer dependencies
# /usr/local/bin/composer self-update
cd /var/www
composer install

# TODO: create db
# mysql -u root -e "CREATE DATABASE buc"

# run cake migration
sh cake/bin/cake Migrations migrate
sh cake/bin/cake Migrations seed

# install dependencies
npm install

# bundle all code and css
npm start