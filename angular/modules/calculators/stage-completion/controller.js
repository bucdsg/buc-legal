module.exports = function(app) {
  app.controller('stageCompletionCalcController', function(
    $scope,
    MilestoneFactory,
    CalculatorsFactory
  ) {
    $scope.dateFormat = 'dd/MM/yyyy'
    $scope.stages = MilestoneFactory.stageCompletionTemplate,
    $scope.submitting = false
    $scope.resultHTML = ''
    $scope.data = {
      completionDate: null,
      cscDate: null,
      finalPaymentDate: null,
      purchasePrice: 0,
      stages: MilestoneFactory.stageCompletionTemplate.map(stage => {
        return {
          ...stage,
          dateNotice: '',
          datePayment: ''
        }
      }),
      deductions: []
    }

    $scope.newDeduction = () => {
      $scope.data.deductions.push({
        amount: 0
      })
    }

    $scope.removeDeduction = (index) => {
      $scope.data.deductions.splice(index, 1)
    }

    $scope.submit = async (form) => {
      if (form.$invalid) return

      try {
        $scope.submitting = true
        $scope.resultHTML = 'Loading...'
        const result = await CalculatorsFactory.stateOfCompletion($scope.data)

        $scope.resultHTML = result.html

      } catch (err) {
        swal({
          text: err,
          type:'error'
        })
        $scope.resultHTML = 'Error'
      } finally {
        $scope.submitting = false
        $scope.$apply()
      }
    }
  })
}
