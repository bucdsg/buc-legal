module.exports = function (app) {
  app.config(function($stateProvider) {
    $stateProvider.state('calculators.stageCompletion', {
      url: '/stage-completion',
      template: require('./stage-completion.html'),
      controller: 'stageCompletionCalcController'
    });
  });


  require('./controller')(app);

};
