module.exports = function (app) {
  app.config(function($stateProvider) {
    $stateProvider.state('calculators', {
      url: '/calculators',
      redirectTo: 'calculators.stageCompletion',
      template: '<ui-view/>'
    });
  });

  require('./stage-completion')(app);
}
