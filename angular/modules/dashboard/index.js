module.exports = function(app) {
  app.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider.state('dashboard', {
      url: '/',
      template: require('./dashboard.html'),
      controller: 'dashboardController',
      resolve: {
        projects: function(ProjectFactory) {
          return ProjectFactory.getAll();
        }
      }
      /* resolve: {
        dashboardStats: function(StatsFactory) {
          return StatsFactory.dashboardStats();
        }
      } */
    });

    /* $stateProvider.state('dashboard.notificationToSend', {
      template: require('./panels/notification-to-send.html')
    });

    $stateProvider.state('dashboard.notificationToResolve', {
      template: require('./panels/notification-to-resolve.html')
    }); */
  });

  require('./controller')(app);
};
