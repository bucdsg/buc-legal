module.exports = function(app) {
  app.controller('dashboardController', function($scope, MilestoneFactory, StatsFactory, projects) {
    $scope.dateFormat = 'dd/MM/yyyy'
    $scope.outstandingPayments = null
    $scope.projects = projects
    $scope.currentProjectId = null
    $scope.currentProjectName = ''
    $scope.chartData = null
    $scope.stackedBar = null
    $scope.chartLoading = true

    $scope.sendNotification = function ({notice}) {
      MilestoneFactory.sendNotice(notice.id)
        .then(() => {
          $state.reload()
        })
    }

    $scope.updateGraph = (projectId) => {
      $scope.currentProjectId = projectId
      $scope.loadPropertyStats()
    }

    $scope.loadPropertyStats = () => {
      if (!$scope.currentProjectId) {
        $scope.chartLoading = false
        return
      }

      $scope.chartLoading = true

      StatsFactory.propertyStageSummary($scope.currentProjectId)
        .then(data => {
          $scope.chartData = data

          let series = [
            [], [], []
          ];

          for (let stageId in data) {
            series[0].push(data[stageId].paid)
            series[1].push(data[stageId].pending)
            series[2].push(data[stageId].created)
          }

          console.log(series)

          if ($scope.stackedBar) {
            $scope.stackedBar.data.series = series
            $scope.stackedBar.update()
          } else {
            $scope.stackedBar = new Chartist.Bar('#chartBarStacked .ct-chart',
              {
                labels: [
                  'Stage 1',
                  'Stage 2',
                  'Stage 3',
                  'Stage 4',
                  'Stage 5',
                  'Stage 6',
                  'Stage 7',
                  'Stage 8'
                ],
                series
              },
              {
                stackBars: true,
                fullWidth: true,
                seriesBarDistance: 0,
                onlyInteger: true,
                chartPadding: {
                  top: -10,
                  right: 0,
                  bottom: 0,
                  left: 0
                },
                axisX: {
                  showLabel: true,
                  showGrid: false,
                  offset: 30,
                },
                axisY: {
                  showLabel: true,
                  showGrid: true,
                  offset: 30,
                  onlyInteger: true
                }
              }
            );
          }

          // update project name on chart
          let currentProject = $scope.projects.find(project => project.id == $scope.currentProjectId);
          console.log('cp', currentProject)
          if (currentProject) {
            $scope.currentProjectName = currentProject.name
          }
          $scope.chartLoading = false
        }, err => {
          alert('Unable to load stats data')
          $scope.chartLoading = false
        })
    }

    $scope.lazyLoadData = () => {
      MilestoneFactory.getUnpaid()
        .then(data => $scope.outstandingPayments = data)

      $scope.currentProjectId = $scope.projects[0].id.toString()
      $scope.currentProjectName = $scope.projects[0].name.toString()

      $scope.loadPropertyStats()
    }

    $scope.lazyLoadData()
  })
}
