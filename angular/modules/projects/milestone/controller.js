module.exports = function(app) {
  app.controller('newMilestoneController', function($scope, $state, ProjectFactory, project, completionItems) {
    function updateUnitSelection () {
      $scope.inputs.selectedUnits = $scope.inputs.selectedUnits.map((unit) => {
        let matcheIds = unit.completions.filter((obj) => {
          return obj.id && obj.completionTypeId === +$scope.inputs.selectedCompletionItem
        });

        return Object.assign({}, unit, {
          disabled: !!matcheIds.length | !unit.client,
          completed: !!matcheIds.length
        })
      })
    }

    $scope.project = project
    $scope.completionItems = completionItems
    $scope.inputs = {
      selectedCompletionItem: "0",
      selectedUnits: project.properties.map((property) => {
        return {
          id: property.id,
          name: property.name,
          completions: property.completions,
          selected: false,
          disabled: true,
          completed: false,
          client: property.client,
          purchase_amount: property.purchase_amount
        }
      })
    }

    $scope.submit = function () {
      const formData = {
        completionId: $scope.inputs.selectedCompletionItem,
        units: $scope.inputs.selectedUnits.filter((unit) => !unit.disabled && unit.selected)
      }

      ProjectFactory.newCompletionItem(project.id, formData)
        .then( data => $state.go('project.view', {id: project.id}) )
        .error( err => alert(err) )
    }

    $scope.$watchCollection('inputs.selectedCompletionItem', (id) => {
      updateUnitSelection()
    });
  })
}
