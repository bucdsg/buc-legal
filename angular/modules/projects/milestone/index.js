module.exports = function (app) {

  app.config(function ($stateProvider) {
    $stateProvider.state('project.newMilestone', {
      url: '/:id/milestone/new',
      template: require('./new-milestone.html'),
      controller: 'newMilestoneController',
      resolve: {
        project: function (ProjectFactory, $stateParams) {
          return ProjectFactory.getById($stateParams.id);
        },
        completionItems: function (ProjectFactory, $stateParams) {
          return ProjectFactory.getCompletionTypes($stateParams.id);
        }
      }
    });
  });

  require('./controller')(app);

};
