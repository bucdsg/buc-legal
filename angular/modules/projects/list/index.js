module.exports = function (app) {

  app.config(function($stateProvider) {
    $stateProvider.state('project.list', {
      template: require('./projects.html'),
      controller: 'projectsController',
      resolve: {
        projects: function(ProjectFactory) {
          return ProjectFactory.getAll();
        }
      }
    });
  });

  require('./controller')(app);
};
