module.exports = function(app) {
  app.controller('projectsController', function($scope, projects) {
    $scope.projects = projects;
  });
};
