module.exports = function (app) {
  app.config(function($stateProvider) {
    $stateProvider.state('project', {
      url: '/project',
      redirectTo: 'project.list',
      template: '<ui-view/>'
    });
  });

  require('./list')(app);
  require('./view')(app);
  require('./new')(app);
  require('./milestone')(app);
}
