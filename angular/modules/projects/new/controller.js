module.exports = function (app) {
  app.controller('newProjectController', function ($scope, ProjectFactory, $state) {
    var processUpdateProjectSize = require('./update-project-size')

    $scope.inputs = {
      numFloors: 10,
      roomsPerFloor: 5,
      unitNamingType: '1',
      namePrefix: '#',
      project: {}
    }

    $scope.showMapping = false

    $scope.updateProjectSize = function () {
      processUpdateProjectSize($scope)
    }

    $scope.addUnitByFloor = function (floor) {
      floor.push('Unit')
    }

    $scope.removeUnit = function (floor, UnitIdx) {
      floor.splice(UnitIdx, 1)
    }

    $scope.submit = function (form) {
      $scope.submitted = true;
      if (form.$invalid) {
          return ;
      }
      ProjectFactory.newProject(this.inputs.project)
        .then( data => $state.go('project.list') )
        .error( err => alert(err) )
    }
  })
}
