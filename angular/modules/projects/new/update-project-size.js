var updateProjectSize = (_scope) => {
  var unitsMap = [];
  var numFloors = parseInt(_scope.inputs.numFloors);
  var roomsPerFloor = parseInt(_scope.inputs.roomsPerFloor);

  if (!numFloors || numFloors < 1) {
    alert('Invalid number of floors');
    return null;
  }

  if (!roomsPerFloor || roomsPerFloor < 1) {
    alert('Invalid number of rooms per floor');
    return null;
  }

  unitsMap = Array(numFloors).fill().map(function (fval, fidx) {
    let roomMap = {};
    let floorIdx = fidx + 1;

    let units = Array(roomsPerFloor).fill().map(function (uval, uidx) {
      let unitName = '';
      let unitIdx = (uidx + 1).pad(2);

      switch (_scope.inputs.unitNamingType) {
        case '3':
          unitName = [_scope.inputs.namePrefix, floorIdx, String.fromCharCode('A'.charCodeAt(0) + uidx)].join('');
          break;
        case '2':
          unitName = [_scope.inputs.namePrefix, floorIdx, unitIdx].join('');
          break;
        case '1':
        default:
          unitName = [_scope.inputs.namePrefix, floorIdx, '-', unitIdx].join('');
          break;
      }

      return unitName;
    });

    roomMap.floorName = floorIdx;
    roomMap.units = units;

    return roomMap;
  });

  _scope.inputs.project.unitsMap = unitsMap;
  _scope.showMapping = true;
}

module.exports = updateProjectSize;
