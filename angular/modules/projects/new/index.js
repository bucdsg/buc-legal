module.exports = function (app) {

  app.config(function($stateProvider) {
    $stateProvider.state('project.new', {
      url: '/new',
      template: require('./new-project.html'),
      controller: 'newProjectController',
    });
  });

  require('./controller')(app);

};
