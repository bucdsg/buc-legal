module.exports = (app) => {
  app.controller('noticeController', ($scope, completions, dateFormat, userSettings) => {
    $scope.dateFormat = dateFormat
    $scope.userSettings = userSettings
    $scope.currentTab = 0

    console.log('completions', completions)
    $scope.completions = completions.map(completion => Object.assign(
      completion,
      {
        hasBankAmount:
          completion.completionItem &&
          completion.completionItem.completion_payments &&
          completion.completionItem.completion_payments.find(cp => cp.mode_of_payment === 'loan'),
        hasCpfAmount:
          completion.completionItem &&
          completion.completionItem.completion_payments &&
          completion.completionItem.completion_payments.find(cp => cp.mode_of_payment.startsWith('cpf')),
        isPaid: completion.completionItem.status != 'paid'
      }
    ))


    $scope.setTab = (index) => $scope.currentTab = index
  })
}
