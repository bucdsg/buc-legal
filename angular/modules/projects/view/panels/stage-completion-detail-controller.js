const $ = require('jquery');
// $uibModalInstance represents a modal window (instance) dependency
module.exports = function(app) {
  app.controller('stageCompletionDetailController', function(
    $scope,
    $filter,
    ProjectFactory,
    $uibModalInstance,
    dateFormat,
    stageUnits,
    userSettings
  ) {
    $scope.dateFormat = dateFormat;
    $scope.stageUnits = stageUnits;
    $scope.numDateLetterToDue = parseInt(userSettings.due_days_to_developer_from_payment);
    $scope.dateOffsetForDue = parseInt(userSettings.payment_due_days_from_due_date_to_developer);
    $scope.userSettings = userSettings;

    console.log(stageUnits);

    $scope.form = {
      letter_date: '',
      date_received: '',
      payment_due_date: '',
      payment_action_date: '',
      action_days_before_due: $scope.userSettings.stage_completion_action_days_from_payment_due_date,
      due_date_to_developer: ''
    };

    $scope.$watch('form.letter_date', function(newValue, oldValue) {
      let dateComp = newValue.split('/');
      let letterDate = new Date(dateComp[2], dateComp[1] - 1, dateComp[0]);
      let dueDateToDeveloper = new Date(letterDate.getTime());
      let paymentDue = new Date(letterDate.getTime());
      let actionDate = new Date(letterDate.getTime());

      dueDateToDeveloper.setDate(letterDate.getDate() + $scope.numDateLetterToDue);
      paymentDue.setDate(letterDate.getDate() + $scope.numDateLetterToDue - $scope.dateOffsetForDue);
      actionDate.setDate(letterDate.getDate() + $scope.numDateLetterToDue - $scope.dateOffsetForDue - parseInt($scope.form.action_days_before_due));

      $scope.form.due_date_to_developer = $filter('date')(dueDateToDeveloper, $scope.dateFormat);
      $scope.form.payment_due_date = $filter('date')(paymentDue, $scope.dateFormat);
      $scope.form.payment_action_date = $filter('date')(actionDate, $scope.dateFormat);
    });

    $scope.save = function () {
      if ($scope.form.letter_date.length <= 0 ||
        $scope.form.date_received <= 0 ) {
        alert('Please enter a valid date');
        return;
      }

      ProjectFactory.setCompletionItems(Object.assign({}, $scope.form, {stageUnits:$scope.stageUnits}));
      console.log('ok', $scope.form);
      $uibModalInstance.close($scope.form);
      location.reload();
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
      console.log('cancel', $scope.form);
    };

    /* $scope.$parent.propertyId = $scope.propertyId = propertyId;
    $scope.$parent.stageCompletionId = $scope.stageCompletionId = stageCompletionId;
    $scope.property = {id: 0, name: ''};
    $scope.stageCompletion = {
      id: 0,
      completionTypeId: 0,
      name: '',
      scheduled_at: null,
      completed_at: null,
      created_at: null,
      description: '',
      notes: '',
      notices: [],
      property_id: propertyId
    };

    for (let currentProperty of $scope.project.properties) {
      if (currentProperty.id == propertyId) {
        $scope.property = currentProperty;
        console.log('prop', currentProperty);

        for (let currentCompletion of currentProperty.completions) {
          if (stageCompletionId == currentCompletion.completionTypeId) {
            $scope.stageCompletion = currentCompletion;
            console.log('comp', currentCompletion);
            $scope.stageCompletion.property_id = $scope.propertyId;
            $scope.stageCompletion.completed_at = $filter('date')(currentCompletion.completed_at, $scope.dateFormat);
            $scope.stageCompletion.scheduled_at = $filter('date')(currentCompletion.scheduled_at, $scope.dateFormat);
            break;
          }
        }

        break;
      }
    }

    $scope.saveStageCompletionDate = function () {
      if ($scope.stageCompletion.completed_at.length <= 0) {
        alert('Please enter completion date');
        return;
      }

      MilestoneFactory.setUnitStageCompletionDate($scope.stageCompletion)
        .then(() => {
          $state.reload();
        });
    }

    $scope.sendNotification = function ({notice}) {
      MilestoneFactory.sendNotice(notice.id)
        .then(() => {
          $state.reload();
        });
    }

    $scope.resendNotification = function ({notice}) {
      MilestoneFactory.resendNotice(notice.id)
        .then(() => {
          $state.reload();
        });
    }

    $scope.markResolve = function ({notice}) {
      MilestoneFactory.markResolve(notice.id)
        .then(() => {
          $state.reload();
        });
    } */
  });
}
