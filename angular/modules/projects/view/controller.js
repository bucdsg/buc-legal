const $ = require('jquery')

module.exports = function(app) {
  app.controller('projectsViewController', function(
    $scope,
    $state,
    $filter,
    $timeout,
    $uibModal,
    MilestoneFactory,
    ProjectFactory,
    project,
    propertiesCompletions,
    completionTypes,
    userSettings
  ) {
    $scope.project = project
    $scope.propertiesCompletions = propertiesCompletions
    $scope.completionTypes = completionTypes
    $scope.stageCompletionList = []
    $scope.dateFormat = 'dd/MM/yyyy'
    $scope.propertyId = null
    $scope.stageCompletionId = null
    $scope.selectedStageCompletionItems = []
    $scope.userSettings = userSettings
    $scope.showWithClientsOnly = true

    // if there is no property assigned to any client, show all units
    if (project.properties && project.properties.length) {
      const hasClient = project.properties.find(prop => prop.client_id)

      if (!hasClient) {
        $scope.showWithClientsOnly = false
      }
    }

    $scope.input = Object.assign({}, project, {
      csc_date: $filter('date')(project.csc_date, $scope.dateFormat),
      completion_date: $filter('date')(project.completion_date, $scope.dateFormat)
    })

    $scope.getClientName = function (property) {
      if (property.client_id) {
        return property.client.name
      }

      return ''
    }

    $scope.isCellActive = function(propertyId, stageId) {
      return $scope.propertyId == propertyId && $scope.stageCompletionId == stageId
    }

    $scope.toggleSelectUnit = function (propertyId, stageTypeId) {
      let packed = [propertyId, stageTypeId].join('|')
      let sIndex = $scope.selectedStageCompletionItems.indexOf(packed)
      if (sIndex >= 0) {
        // already selected, selected it
        $scope.selectedStageCompletionItems.splice(sIndex, 1)
      } else {
        $scope.selectedStageCompletionItems.push(packed)
      }
      $scope.buildStageCompletion()
    }

    $scope.showStageCompletionModal = function () {
      const selectedUnitsPerStage = $scope.getSelectedUnitsPerStage();

      // check if the user selected a unit completion with existing letter date
      if (selectedUnitsPerStage.find(stage => stage.units.find(unit => unit.hasLetterDate))) {
        alert('Please only select stage completion without completion date')
        return
      }

      if ($scope.selectedStageCompletionItems.length) {
        var modalInstance = $uibModal.open({
          animation: false,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          template: require('./panels/stage-completion-detail.html'),
          controller: 'stageCompletionDetailController',
          size: 'lg',
          resolve: {
            userSettings: () => $scope.userSettings,
            dateFormat: () => $scope.dateFormat,
            stageUnits: () => selectedUnitsPerStage
          }
        })

        modalInstance.result.then(function (val) {
          console.log('modal ok, back to main page', val)
        }, function () {
          console.log('modal dismissed')
        })
      }
    }

    $scope.getSelectedUnitsPerStage = () => {
      const completedStatus = ['paid', 'pending']
      let groupByStages = {}

      for (let packed of $scope.selectedStageCompletionItems) {
        let gArr = packed.split('|')
        let unitId = gArr[0]
        let stageId = gArr[1]

        // initialize new array
        if (typeof groupByStages[stageId] === typeof undefined) {
          groupByStages[stageId] = {
            'stageId': stageId,
            'units': []
          }
        }

        // get unit details
        let foundUnit = $scope.propertiesCompletions.find(unit => unit.id == unitId)

        // add to existing array
        if (foundUnit) {
          let unitCompletionItem = foundUnit.property_completions.find(({completion_type_id}) => completion_type_id == stageId)
          groupByStages[stageId].units.push(Object.assign({}, foundUnit, {
            hasLetterDate:  unitCompletionItem ? completedStatus.indexOf(unitCompletionItem.status) !== -1  : false
          }))
        }
      }
      return Object.keys(groupByStages).map(key => groupByStages[key])
    }

    $scope.showNoticeModal = () => {
      if ($scope.selectedStageCompletionItems.length) {
        var modalInstance = $uibModal.open({
          animation: false,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          template: require('./panels/notice.html'),
          controller: 'noticeController',
          size: 'lg',
          resolve: {
            userSettings: () => $scope.userSettings,
            dateFormat: () => $scope.dateFormat,
            completions: () => $scope.getSelectedCompletions()
          }
        })

        modalInstance.result.then(function (val) {
          console.log('modal ok, back to main page', val)
        }, function () {
          console.log('modal dismissed')
        })
      }
    }

    $scope.getSelectedCompletions = () => {
      const completedStatus = ['paid', 'pending']
      let completions = []

      for (let packed of $scope.selectedStageCompletionItems) {
        let gArr = packed.split('|')
        let unitId = gArr[0]
        let stageId = gArr[1]

        // get unit details
        let foundUnit = $scope.propertiesCompletions.find(unit => unit.id == unitId)

        // add to existing array
        if (foundUnit) {
          let foundCompletionItem = foundUnit.property_completions.find(({completion_type_id}) => completion_type_id == stageId)
          completions.push(Object.assign({}, foundUnit, {completionItem: foundCompletionItem}))
        }
      }

      return Object.keys(completions).map(key => completions[key])
    }

    $scope.clearUnitSelections = function () {
      $scope.selectedStageCompletionItems = []
      $scope.buildStageCompletion()
    }

    /**
     * Build stage completion array to used in the table.
     * This will include the name, stage id, and selected option
     */
    $scope.buildStageCompletion = function () {
      let result = []

      for (let property of $scope.propertiesCompletions) {
        let propertyRow = {
          id: property.id,
          name: property.name,
          clientName: property.client && property.client.name,
          completions: []
        }

        for (let type of $scope.completionTypes) {
          let currentCompletion = {
            id: null,
            typeId: type.id,
            date: '',
            selected: false,
            notes: '',
            status: ''
          }

          for (let completionItem of property.property_completions) {
            if (!currentCompletion.selected) {
              let packed = [property.id, type.id].join('|')
              let sIndex = $scope.selectedStageCompletionItems.indexOf(packed)

              if (sIndex >= 0) {
                currentCompletion.selected = true
              }
            }

            if (type.id === completionItem.completion_type_id) {
              currentCompletion.date = $filter('date')(completionItem.letter_date, $scope.dateFormat)
              currentCompletion.scheduledDate = $filter('date')(completionItem.scheduled_at, $scope.dateFormat)
              currentCompletion.id = completionItem.id
              currentCompletion.notes = completionItem.notes
              currentCompletion.status = completionItem.status
            }
          }

          propertyRow.completions.push(currentCompletion)
        }

        result.push(propertyRow)
      }

      console.log(result)
      $scope.stageCompletionList = result
    }

    $scope.updateProject = () => {
      const data = {
        name: $scope.input.name,
        description: $scope.input.description,
        address: $scope.input.address,
        csc_date: $scope.input.csc_date,
        completion_date: $scope.input.completion_date,
        developer_name: $scope.input.developer_name
      }

      ProjectFactory.updateProjectDetails($scope.project.id, data)
        .then(() => location.reload(), () => alert('Unable to save project details'))
    }

    $scope.buildStageCompletion()
  })
}
