module.exports = (app) => {
  app.directive('propertyStageDots', () => {
    return {
      template: require('./property-stage-dots.html'),
      controller: ($scope) => {
        const excludedStageId = [100, 101]
        $scope.completionItems = $scope.property.property_completions
          .filter(completion => excludedStageId.indexOf(completion.completion_type_id) === -1)
          .sort((a, b) => a.completion_type_id > b.completion_type_id)
      }
    }
  })
}
