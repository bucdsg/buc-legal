module.exports = function (app) {

  app.config(function($stateProvider) {
    $stateProvider.state('project.view', {
      url: '/:id',
      redirectTo: 'project.view.details',
      template: require('./project-view.html'),
      controller: 'projectsViewController',
      resolve: {
        project: (ProjectFactory, $stateParams) => ProjectFactory.getById($stateParams.id),
        propertiesCompletions: (ProjectFactory, $stateParams) => ProjectFactory.propertiesAndCompletions($stateParams.id),
        completionTypes: (ProjectFactory, $stateParams) => ProjectFactory.getCompletionTypes(),
        userSettings: (SettingsFactory) => SettingsFactory.getUserSettings()
      }
    });

    $stateProvider.state('project.view.details', {
      url: '/details',
      template: require('./panels/project-details.html')
    });

    $stateProvider.state('project.view.stageCompletion', {
      url: '/stage-completions',
      template: require('./panels/stage-completions.html')
    });

    $stateProvider.state('project.view.allUnits', {
      url: '/all-units',
      template: require('./panels/all-units.html')
    });

    /* $stateProvider.state('project.view.stageCompletion.detail', {
      url: '/stage/:stageId/property/:propertyId',
      template: require('./panels/stage-completion-detail.html'),
      controller: 'stageCompletionDetailController',
      resolve: {
        propertyId: function($stateParams) {
          return $stateParams.propertyId;
        },
        stageCompletionId: function($stateParams) {
          return $stateParams.stageId;
        }
      }
    }); */
  });

  require('./controller')(app);
  require('./panels/stage-completion-detail-controller')(app);
  require('./panels/notice-controller')(app);
  require('./directives/property-stage-dots')(app);

};
