module.exports = function (app) {
  app.config(function($stateProvider) {
    $stateProvider.state('payments', {
      url: '/payments?propertyId',
      redirectTo: 'payments.index',
      template: '<ui-view/>'
    });
  });

  require('./payment-index')(app);
}
