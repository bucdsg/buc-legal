module.exports = (app) => {
  app.directive('stageCompletionSummary', () => {
    return {
      template: require('./stage-completion-summary.html')
    }
  })
}
