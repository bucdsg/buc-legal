const $ = require('jquery')
// $uibModalInstance represents a modal window (instance) dependency
module.exports = function(app) {
  app.controller('setPaymentcontroller', function($scope, $uibModalInstance, MilestoneFactory, paymentItem, dateFormat) {
    $scope.paymentItem = paymentItem
    $scope.dateFormat = dateFormat
    $scope.completionItem = MilestoneFactory.getStageCompletionTemplateById(paymentItem.completion_type_id)
    $scope.paymentAmount = 0

    if ($scope.completionItem) {
      $scope.paymentAmount = $scope.completionItem.percentage * $scope.paymentItem.property.purchase_amount
    }

    $scope.form = {
      payment_date: ''
    }

    $scope.submitForm = () => {
      if ($scope.form.payment_date.length <= 0) {
        alert('Please enter payment date')
        return
      }

      MilestoneFactory.setPaymentDate(paymentItem.id, $scope.form.payment_date)
        .then( data => location.reload() )
        .error( err => alert(err) )
    }

    $scope.cancel = () => $uibModalInstance.dismiss()
  })
}
