const $ = require('jquery')
// $uibModalInstance represents a modal window (instance) dependency
module.exports = function(app) {
  app.controller('followupController', function($scope, $uibModalInstance, MilestoneFactory, paymentItem, dateFormat) {
    $scope.paymentItem = paymentItem
    $scope.dateFormat = dateFormat
    $scope.completionItem = MilestoneFactory.getStageCompletionTemplateById(paymentItem.completion_type_id)
    $scope.paymentAmount = 0
    $scope.sendTo = ''
    $scope.message = ''
    $scope.sendOption = 'email'

    if ($scope.completionItem) {
      $scope.paymentAmount = $scope.completionItem.percentage * $scope.paymentItem.property.purchase_amount
    }

    $scope.submitForm = () => {
      if ($scope.sendTo.length == 0) return alert('Please select recipient')
      if ($scope.message.length == 0) return alert('Please enter a message')
      alert('Email sent')
    }

    $scope.cancel = () => $uibModalInstance.dismiss()
  })
}
