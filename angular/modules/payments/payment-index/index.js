module.exports = function (app) {

  app.config(function($stateProvider) {
    $stateProvider.state('payments.index', {
      template: require('./payment-index.html'),
      controller: 'paymentsController',
      resolve: {
        paymentItems: function (MilestoneFactory) {
          return MilestoneFactory.getUnpaid();
        }
      }
    });
  });

  require('./controller')(app);
  require('./modals/set-payment-controller')(app)
  require('./modals/follow-up-controller')(app)
  require('./directives/stage-completion-summary')(app)
};
