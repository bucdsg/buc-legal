module.exports = function(app) {
  app.controller('paymentsController', function($scope, $uibModal, $stateParams, paymentItems) {
    $scope.paymentItems = paymentItems
    $scope.dateFormat = 'dd/MM/yyyy'
    $scope.selectedPropertyId = $stateParams.propertyId
    console.log($stateParams, $scope.selectedPropertyId)

    $scope.setPayment = function(paymentItem) {
      var modalInstance = $uibModal.open({
        animation: false,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        template: require('./modals/set-payment.html'),
        controller: 'setPaymentcontroller',
        size: 'md',
        resolve: {
          dateFormat: function () {
            return $scope.dateFormat
          },
          paymentItem: () => paymentItem
        }
      })

      modalInstance.result.then(function (val) {
        console.log('modal ok, back to main page', val)
      }, function () {
        console.log('modal dismissed')
      })
    }

    $scope.followUp = function(paymentItem) {
      var modalInstance = $uibModal.open({
        animation: false,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        template: require('./modals/follow-up.html'),
        controller: 'followupController',
        size: 'md',
        resolve: {
          dateFormat: function () {
            return $scope.dateFormat
          },
          paymentItem: () => paymentItem
        }
      })

      modalInstance.result.then(function (val) {
        console.log('modal ok, back to main page', val)
      }, function () {
        console.log('modal dismissed')
      })
    }
  })
}
