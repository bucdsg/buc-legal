module.exports = function (app) {
  require('./dashboard')(app);
  require('./projects')(app);
  require('./property')(app);
  require('./clients')(app);
  require('./payments')(app);
  require('./calculators')(app);
  require('./settings')(app);
}
