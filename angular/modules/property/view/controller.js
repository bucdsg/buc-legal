module.exports = function(app) {
  app.controller('propertyViewController', function(
    $state,
    $scope,
    $uibModal,
    PropertyFactory,
    MilestoneFactory,
    property,
    userSettings
  ) {
    $scope.dateFormat = 'dd/MM/yyyy'
    $scope.property = property
    $scope.formData = {
      clientName: '',
      clientAddress: '',
      purchasePrice: property.purchase_amount | 0,
      cashAmount: property.cash_amount | 0,
      loanAmount: property.loan_amount | 0,
      cpf1Name: property.cpf_1_name | '',
      cpf2Name: property.cpf_2_name | '',
      cpf3Name: property.cpf_3_name | '',
      cpf1Amount: property.cpf_1_amount | 0,
      cpf2Amount: property.cpf_2_amount | 0,
      cpf3Amount: property.cpf_3_amount | 0,
      warningMsg: null
    }

    // set certain stage completion as done since the client already paid
    // these completions even before they entered into our system
    $scope.doneStages = [100, 101]

    $scope.milestoneTemplate = MilestoneFactory.stageCompletionTemplate

    // check for stage completion statuses
    $scope.milestoneTemplate = $scope.milestoneTemplate.map((stageItem) => {
      let status = 'pending'
      let milestone = Object.assign({}, stageItem)

      let found = property.property_completions.find((completionItem) => {
        return completionItem.completion_type_id == stageItem.id
      })

      // a unit with no client assigned therefore all status is new
      if (!$scope.property.client_id) {
        return Object.assign({}, milestone, {status})
      }

      if ($scope.doneStages.indexOf(milestone.id) >= 0) {
        status = 'done'
      }

      if (found) {
        milestone = Object.assign({}, milestone, {
          completion_type_id: found.completion_type_id,
          created_at: found.created_at,
          due_date_to_developer: found.due_date_to_developer,
          letter_date: found.letter_date,
          letter_received_date: found.letter_received_date,
          notes: found.notes,
          payment_action_date: found.payment_action_date,
          payment_due_date: found.payment_due_date,
          property_completion_type: found.property_completion_type,
          property_id: found.property_id,
          scheduled_at: found.scheduled_at,
        })

        // if stage completion is found and status == paid or
        // if stage completion type id is 100 or 101
        if (found.status == 'paid') {
          status = 'done'
        } else if (found.status == 'pending'){
          status = 'waiting'
        }
      }

      return Object.assign({}, milestone, {status})
    })

    // check for warnings
    $scope.propertyWarnings = []

    $scope.milestoneTemplate.forEach((milestone) => {
      // check if it has already completed and
      // check if due date to develop has passed
      if (milestone.letter_date) {
        try {
          const developerDate = new Date(milestone.due_date_to_developer)
          const today = new Date()

          if (today > developerDate) {
            $scope.propertyWarnings.push({
              'title': 'Subject to contract termination',
              'text': `Stage ${milestone.name} is past due`
            })
          }
        } catch (err) {
          console.log('Error parsing date')
        }
      }
    })

    $scope.mergeMilestoneTemplateAndData = (milestone) => {
      const projectMilestone = $scope.property.property_completions.find(p => p.completion_type_id == milestone.id)

      return {
        ...milestone,
        ...projectMilestone,
        id: projectMilestone && projectMilestone.id
      }
    }

    $scope.onClickMilestone = (milestone) => {
      const milestoneStageData = $scope.mergeMilestoneTemplateAndData(milestone)
      const parseMilestone = (milestone) => ([{
        ...milestoneStageData,
        ...{
          name: $scope.property.name,
          project: $scope.property.project,
          client: $scope.property.client,
          completionItem: {
            ...milestoneStageData
          }
        }
      }])
      console.log('aa', milestone)
      console.log('dd', milestoneStageData)
      console.log('mm', parseMilestone(milestone))
      console.log('pp', $scope.property)

      var modalInstance = $uibModal.open({
        animation: false,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        template: require('../../projects/view/panels/notice.html'),
        controller: 'noticeController',
        size: 'lg',
        resolve: {
          userSettings: () => $scope.userSettings,
          dateFormat: () => $scope.dateFormat,
          completions: () => parseMilestone(milestone)
        }
      })

      modalInstance.result.then(function (val) {
        console.log('modal ok, back to main page', val)
      }, function () {
        console.log('modal dismissed')
      })
    }

    $scope.getPerentageAmount = function (amt, percent) {
      return parseFloat(amt) * parseFloat(percent)
    }

    $scope.submit = function (form) {

      $scope.submitted = true
      if (form.$invalid) {
          return
      }

      PropertyFactory.assignToNewClient(property.id, Object.assign({}, this.formData, {stages: $scope.milestoneTemplate}))
        .then( data => $state.reload() )
    }

    $scope.$watchCollection(
      'formData',
      function(newValue, oldValue) {
        var purchasePrice = parseFloat(newValue.purchasePrice)
        // editable value
        var paymentSources = {
          cashAmount: parseFloat(newValue.cashAmount),
          loanAmount: parseFloat(newValue.loanAmount),
          cpf1Amount: parseFloat(newValue.cpf1Amount),
          cpf2Amount: parseFloat(newValue.cpf2Amount),
          cpf3Amount: parseFloat(newValue.cpf3Amount)
        }

        var paymentApplied = {
          cashAmount: 0,
          loanAmount: 0,
          cpf1Amount: 0,
          cpf2Amount: 0,
          cpf3Amount: 0
        }

        const calculateCashAmount = ({ amount, paymentSources, paymentApplied }) => {
          var cashAmount = parseFloat(paymentSources.cashAmount)
          var applyCashAmount = 0
          var newAmount = parseFloat(amount)

          if (cashAmount > 0) {
            var tryCashAmount = cashAmount
            tryCashAmount -= newAmount

            if (tryCashAmount < 0) {
              applyCashAmount = cashAmount
              newAmount -= applyCashAmount
            } else {
              applyCashAmount = newAmount
              newAmount = 0
            }

            cashAmount -= applyCashAmount
          }

          var newApplied = Object.assign({}, paymentApplied, {
            cashAmount: applyCashAmount
          })
          var newSources = Object.assign({}, paymentSources, {
            cashAmount: cashAmount
          })
          return { amount, newSources, newApplied }
        }

        const getAmountLeft = (milestoneItem) => {
          var runningAmount = parseFloat(milestoneItem.amount)
          var paidAmount = [
            milestoneItem.cashAmount,
            milestoneItem.loanAmount,
            milestoneItem.cpf1Amount,
            milestoneItem.cpf2Amount,
            milestoneItem.cpf3Amount
          ].reduce((sum, value) => sum + value, 0)

          return runningAmount - paidAmount
        }

        // Init values
        $scope.milestoneTemplate = $scope.milestoneTemplate.map((item) => {
          var amount = purchasePrice * item.percentage
          return Object.assign({}, item, { amount }, paymentApplied)
        })

        // Calculate Cash Amount
        $scope.milestoneTemplate = $scope.milestoneTemplate.map((item) => {
          var calculated = calculateCashAmount({ amount: item.amount, paymentSources, paymentApplied })
          paymentSources = calculated.newSources
          paymentApplied = calculated.newApplied

          return Object.assign({}, item, { cashAmount: paymentApplied.cashAmount })
        })

        // Calculate cpf1 amount
        $scope.milestoneTemplate = $scope.milestoneTemplate.map((item) => {
          var cpf1Amount = 0

          if (paymentSources.cpf1Amount) {
            var amountLeft = getAmountLeft(item)
            if (amountLeft > paymentSources.cpf1Amount) {
              cpf1Amount = paymentSources.cpf1Amount
            } else {
              cpf1Amount = amountLeft
            }

            paymentSources = Object.assign({}, paymentSources, {
              cpf1Amount: paymentSources.cpf1Amount - cpf1Amount
            })
            paymentApplied = Object.assign({}, paymentApplied, {
              cpf1Amount
            })
          }

          return Object.assign({}, item, { cpf1Amount })
        })

        // Calculate cpf2 amount
        $scope.milestoneTemplate = $scope.milestoneTemplate.map((item) => {
          var cpf2Amount = 0

          if (paymentSources.cpf2Amount) {
            var amountLeft = getAmountLeft(item)
            if (amountLeft > paymentSources.cpf2Amount) {
              cpf2Amount = paymentSources.cpf2Amount
            } else {
              cpf2Amount = amountLeft
            }

            paymentSources = Object.assign({}, paymentSources, {
              cpf2Amount: paymentSources.cpf2Amount - cpf2Amount
            })
            paymentApplied = Object.assign({}, paymentApplied, {
              cpf2Amount
            })
          }

          return Object.assign({}, item, { cpf2Amount })
        })

        // Calculate cpf3 amount
        $scope.milestoneTemplate = $scope.milestoneTemplate.map((item) => {
          var cpf3Amount = 0

          if (paymentSources.cpf3Amount) {
            var amountLeft = getAmountLeft(item)
            if (amountLeft > paymentSources.cpf3Amount) {
              cpf3Amount = paymentSources.cpf3Amount
            } else {
              cpf3Amount = amountLeft
            }

            paymentSources = Object.assign({}, paymentSources, {
              cpf3Amount: paymentSources.cpf3Amount - cpf3Amount
            })
            paymentApplied = Object.assign({}, paymentApplied, {
              cpf3Amount
            })
          }

          return Object.assign({}, item, { cpf3Amount })
        })

        // Calculate Loan Amount
        $scope.milestoneTemplate = $scope.milestoneTemplate.map((item) => {
          var loanAmount = 0

          if (paymentSources.loanAmount) {
            var amountLeft = getAmountLeft(item)
            if (amountLeft > paymentSources.loanAmount) {
              loanAmount = paymentSources.loanAmount
            } else {
              loanAmount = amountLeft
            }

            paymentSources = Object.assign({}, paymentSources, {
              loanAmount: paymentSources.loanAmount - loanAmount
            })
            paymentApplied = Object.assign({}, paymentApplied, {
              loanAmount
            })
          }

          return Object.assign({}, item, { loanAmount })
        })

        var $hasError = $scope.milestoneTemplate.reduce((res, item) => {
          var amountLeft = getAmountLeft(item)

          if (amountLeft)
            return true
          else
            return res
        }, false)

        $scope.formData.warningMsg = $hasError ? 'Amount not equal to purchase price' : null
      }
    )
  })
}
