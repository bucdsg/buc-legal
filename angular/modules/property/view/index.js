module.exports = function (app) {

  app.config(function($stateProvider) {
    $stateProvider.state('property.view', {
      url: '/:id',
      template: require('./property-view.html'),
      controller: 'propertyViewController',
      resolve: {
        property: function(PropertyFactory, $stateParams) {
          return PropertyFactory.getById($stateParams.id);
        },
        userSettings: (SettingsFactory) => SettingsFactory.getUserSettings()
      }
    });
  });

  require('./controller')(app);
  require('../../projects/view/panels/notice-controller')(app);

};
