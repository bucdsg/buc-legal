module.exports = function (app) {
  app.config(function($stateProvider) {
    $stateProvider.state('property', {
      url: '/property',
      template: '<ui-view/>'
    });
  });

  require('./view')(app);
}
