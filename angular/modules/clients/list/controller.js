module.exports = function(app) {
  app.controller('clientsController', function($scope, $uibModal, clients) {
    $scope.clients = clients

    $scope.setActivityModal = function(client) {
      var modalInstance = $uibModal.open({
        animation: false,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        template: require('./modals/save-activity-modal.html'),
        controller: 'saveActivityModal',
        size: 'md',
        resolve: {
          activityObject: function () {
            return {
              object: 'client',
              id: client.id
            }
          }
        }
      })

      modalInstance.result.then(function (val) {
        console.log('modal ok, back to main page', val)
      }, function () {
        console.log('modal dismissed')
      })
    }
  })
}
