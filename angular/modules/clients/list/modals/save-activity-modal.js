const $ = require('jquery')
// $uibModalInstance represents a modal window (instance) dependency
module.exports = function(app) {
  app.controller('saveActivityModal', function($scope, $uibModalInstance, activityObject, ActivityFactory) {
    $scope.activityObject = activityObject
    $scope.activityType = ''
    $scope.activityDetails = ''
    $scope.activityDue = ''
    $scope.dateFormat = 'dd/MM/yyyy'

    $scope.submitForm = () => {
      if ($scope.activityType.length == 0) {
        alert('Please select activity type')
        return
      }

      const data = {
        type: $scope.activityType,
        details: $scope.activityDetails,
        activityObject: activityObject,
        dueDate: $scope.activityDue
      }

      ActivityFactory.saveActivity(data)
        .then(() => {
          alert('Activity Saved')
          $uibModalInstance.dismiss()
        }, () => {
          alert('Unable to save')
        })
    }

    $scope.cancel = () => $uibModalInstance.dismiss()
  })
}
