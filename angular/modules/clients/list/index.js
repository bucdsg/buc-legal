module.exports = function (app) {

  app.config(function($stateProvider) {
    $stateProvider.state('client.list', {
      template: require('./clients.html'),
      controller: 'clientsController',
      resolve: {
        clients: function(ClientFactory) {
          return ClientFactory.getAll();
        }
      }
    });
  });

  require('./controller')(app);
  require('./modals/save-activity-modal')(app)
};
