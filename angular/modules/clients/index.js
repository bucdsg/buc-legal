module.exports = function (app) {
  app.config(function($stateProvider) {
    $stateProvider.state('client', {
      url: '/client',
      redirectTo: 'client.list',
      template: '<ui-view/>'
    });
  });

  require('./list')(app);
  require('./view')(app);
  //require('./new')(app);
}
