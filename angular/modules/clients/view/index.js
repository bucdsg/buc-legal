module.exports = function (app) {
  app.config(function($stateProvider) {
    $stateProvider.state('client.view', {
      url: '/:id',
      template: require('./client-view.html'),
      controller: 'clientsViewController',
      resolve: {
        client: function(ClientFactory, $stateParams) {
          return ClientFactory.getById($stateParams.id);
        },
        activities: function(ActivityFactory, $stateParams) {
          return ActivityFactory.getActivities({
            object: 'client',
            id: $stateParams.id
          })
        }
      }
    });
  });

  require('./controller')(app);

};
