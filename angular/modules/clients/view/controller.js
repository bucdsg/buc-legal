module.exports = function(app) {
  app.controller('clientsViewController', function($scope, $state, client, ClientFactory, activities) {
    $scope.client = client;
    $scope.activities = activities;

    $scope.input = {
      id: client.id,
      name: client.name,
      email: client.email,
      address: client.address,
      telephone:client.telephone,
      fax:client.fax,
      citizenship: client.citizenship,
      passport: client.passport,
      spouse_name: client.spouse_name
    };


    $scope.updateClient = function () {
      ClientFactory.updateClient(this.input)
        .then( data => $state.go('client.list') )
        .error( err => alert(err) )
    }

  });
};
