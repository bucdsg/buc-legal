module.exports = function (app) {
  app.config(function($stateProvider) {
    $stateProvider.state('settings.documents', {
      url: '/documents',
      template: require('./document-templates.html'),
      controller: 'documentTemplatesController',
      resolve: {
        templates: (SettingsFactory) => SettingsFactory.getDocumentTemplates()
      }
    });
  });

  require('./controller')(app);
}
