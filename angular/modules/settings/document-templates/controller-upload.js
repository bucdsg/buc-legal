module.exports = function({$scope, $timeout, Upload}) {
  console.log('init uploadDoc')
  $scope.docFile = {}
  $scope.uploadDoc = (file) => {
    file.result = {
      message: '',
      status: 'sending'
    }

    file.upload = Upload.upload({
      url: 'docs/upload',
      data: {
        doc_type: 'template_document',
        doc_vars: {
          template_key: $scope.currentDoctype.key
        },
        file
      }
    })

    file.upload.then((response) => {
      $timeout(() => {
        file.result = response.data

        if (file.result.status === 'success' && file.result.data.success) {
          $scope.currentTemplateFile = file.result.data.filename
        }
      })
    }, (err) => {
      file.result.status = 'error'
      file.result.message = 'Network Error'
    }, (evt) => {
      console.log('ev', evt)
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total))
    })
  }
}
