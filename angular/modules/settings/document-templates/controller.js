module.exports = function(app) {
  app.controller('documentTemplatesController', function($scope, $timeout, Upload, SettingsFactory, templates) {
    $scope.templates = []

    for (let template of templates) {
      $scope.templates[template.type] = {
        'source': template.soure,
        'html': template.html,
        'file': template.file
      }
    }

    $scope.documentTypes = [
      {'key':'notice_to_client', 'name':'Notice to Client', 'description': ''},
      {'key':'notice_to_bank', 'name':'Notice to Bank', 'description': ''},
      {'key':'notice_to_cpf', 'name':'Notice to CPF', 'description': ''},
      {'key':'letter_of_authority_client_property', 'name':'Letter of Authority', 'description': ''},
      {'key':'unit_pre_calculation', 'name':'Unit Pre Calculation', 'description': ''},
      {'key':'letter_to_sal', 'name':'Letter to SAL', 'description': ''}
    ]

    $scope.documentSaving = false
    $scope.templateHasChage = false
    $scope.currentCursor = 0

    // object {key: <string>, name: <string>, description: <string>}
    $scope.currentDoctype = $scope.documentTypes[0]

    $scope.currentTemplateSource = ''
    $scope.currentTemplateFile = ''
    $scope.selectedVariableKey = '0'

    $scope.docVariables = [
      {'key':'date','desc':'Current Date'},
      {'key':'stage_id','desc':'Stage ID'},
      {'key':'amount','desc':'Payment Amount'},
      {'key':'client_name','desc':'Client Name'},
      {'key':'client_email','desc':'Client Email'},
      {'key':'client_address','desc':'Client Address'},
      {'key':'project_name','desc':'Project Name'},
      {'key':'project_address','desc':'Project Address'},
      {'key':'unit_name','desc':'Unit Name'},
      {'key':'developer_due_date','desc':'Due date to developer'},
      {'key':'developer_solicitor_letter_date','desc':'Letter Date'},
      {'key':'payment_due_date','desc':'Payment Due Date'},
      {'key':'completion_name','desc':'Stage Completion Name'},
      {'key':'completion_description','desc':'Stage Completion Description'}
    ];

    var Qeditor
    const Quill = require('quill/dist/quill')
    require('quill/dist/quill.core.css')
    require('quill/dist/quill.snow.css')
    require('./controller-upload')({$scope, $timeout, Upload})

    $scope.setCurrentTemplateSource = (source) => {
      try {
        $scope.templates[$scope.currentDoctype.key].source = source
        $scope.currentTemplateSource = source
      } catch (e) {
        alert('Unable to set template source')
      }
    }

    $scope.newTemplateInstance = (templateType) => {
      return {
        file: null,
        html: '',
        soure: 'html',
        type: templateType
      }
    }

    $scope.getTemplateByKey = (docKey) => {
      let documentType = $scope.documentTypes.find(doctype => doctype.key === docKey)
      let currentTemplate

      if (!documentType) {
        return null
      }

      try {
        currentTemplate = $scope.templates[docKey]
      } catch (e) {
        currentTemplate = null
      }

      if (!currentTemplate) {
        // the template may not exist in user settings
        // create new item in $scope.templates
        currentTemplate = $scope.newTemplateInstance(documentType)
        $scope.templates[docKey] = currentTemplate
      }

      return currentTemplate
    }

    $scope.submitTemplates = () => {
      var templates = {}
      var templateContent = Qeditor.root.innerHTML

      // show page spinner
      $scope.documentSaving = true

      // build template object
      // TODO: Improve me
      templates[$scope.currentDoctype.key] = templateContent

      SettingsFactory.setDocumentTemplates({templates})
        .then(res => {
          // current template save to scope variable
          $scope.templates[$scope.currentDoctype.key].html = templateContent
          $scope.templateHasChage = false
          $scope.documentSaving = false
          alert('Template saved')
        }, err => {
          alert('Unable to save template')
          $scope.documentSaving = false
        })
    }

    $scope.submitTemplateFile = () => {
      console.log('submitting file')
    }

    // Add variable text to current editor cursor position
    $scope.addVariable = () => {
      const varText = '${' + $scope.docVariables[$scope.selectedVariableKey].key + '}'

      if ($scope.currentTemplateSource === 'file') {
        alert(`Add this text to your template file ${varText}`)
      } else {
        Qeditor.insertText($scope.currentCursor, varText)
      }
    }

    // Switch document type and fill corresponding template to editor
    $scope.switchDocType = (docTypeKey) => {
      if (
        $scope.templateHasChage &&
        !confirm(`You have unsaved changes are you sure you want to switch template?`)
      ) {
        return
      }

      // find the current template
      let currentDocType = $scope.documentTypes.find(doctype => doctype.key === docTypeKey)

      if (!currentDocType) {
        alert('Unable to switch template')
      } else {
        // getting the template
        const currentTemplate = $scope.getTemplateByKey(docTypeKey)
        let currentHtml = ''
        let currentSource = 'html'
        let currentTemplateFile = ''

        // check if template is valid
        if (!currentTemplate) {
          alert('Invalid template type')
        } else {
          currentHtml = currentTemplate.html
          currentSource = currentTemplate.source
          currentTemplateFile = currentTemplate.file
        }

        // Set editor content to current template
        Qeditor.clipboard.dangerouslyPasteHTML(currentHtml)

        // Set other attributes
        $scope.templateHasChage = false
        $scope.currentDoctype = currentDocType
        $scope.currentTemplateSource = currentSource
        $scope.currentTemplateFile = currentTemplateFile
      }
    }

    // Setup editor config and events
    $scope.initializeEditors = () => {
      Qeditor = window.documentTemplateEditor = new Quill('#editor', {
        theme: 'snow'
      })

      Qeditor
        // When cursor is moved, save it's location to `$scope.currentCursor`
        .on('selection-change', (range, oldRange, source) => {
          if (range) {
            $scope.currentCursor = range.index
          }
        })
        // When editor content is change mark `templateHasChage` to `true`
        .on('text-change', (delta, source) => {
          $scope.templateHasChage = true
        })

      // select first template in list (default)
      $scope.switchDocType($scope.currentDoctype.key)
    }

    // Initialize the editor
    $scope.initializeEditors()
  })
}
