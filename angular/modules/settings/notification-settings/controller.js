module.exports = function(app) {
  app.controller('notificationSettingsController', function($scope, UserSettings, SettingsFactory) {
    $scope.userSettings = UserSettings
    $scope.selections = {
      'actionDays': Array.from(Array(11).keys()),
      'paymentDueDate': Array.from(Array(21).keys()),
      'dueToDeveloper': Array.from(Array(21).keys())
    }

    $scope.submitForm = () => SettingsFactory.saveUserSettings($scope.userSettings).then(
      () => alert('Settings saved'),
      () => alert('Unable to save changes')
    )
  })
}
