module.exports = function (app) {
  app.config(function($stateProvider) {
    $stateProvider.state('settings.notification', {
      template: require('./notification-settings.html'),
      controller: 'notificationSettingsController',
      resolve: {
        UserSettings: (SettingsFactory) => SettingsFactory.getUserSettings()
      }
    });
  });

  require('./controller')(app);
}
