module.exports = function (app) {
  app.config(function($stateProvider) {
    $stateProvider.state('settings', {
      url: '/settings',
      template: require('./settings.html'),
      redirectTo: 'settings.notification'
    });
  });

  require('./notification-settings')(app);
  require('./document-templates')(app);
}
