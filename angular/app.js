require('angular-ui-router');
require('angular-ui-router.statehelper');
require('angularjs-datepicker');
require('angularjs-datepicker/dist/angular-datepicker.css');
require('ng-file-upload');

const angular = require('angular');

const bucApp = angular.module('bucApp', [
	'ui.router',
  'ui.router.stateHelper',
  '720kb.datepicker',
  'ngFileUpload',
  require('angular-ui-bootstrap/src/popover'),
  require('angular-ui-bootstrap/src/datepicker'),
  require('angular-ui-bootstrap/src/modal'),
  require('angular-ui-bootstrap/src/dropdown')
]);

require('./commons')(bucApp);
require('./factories')(bucApp);
require('./modules')(bucApp);

// default route
bucApp.config(function($stateProvider, $urlRouterProvider) {
  // set  default route
  $urlRouterProvider.otherwise('/');
}).run(['$state', '$rootScope', function($state, $rootScope) {
  // Create our custom redirect function, check for redirect to
  // parameter in route, prevent default executions and just go there
  $rootScope.$on('$stateChangeStart', function(evt, to, params) {
    console.log('detected change state');
    if (to.redirectTo) {
      evt.preventDefault();
      $state.go(to.redirectTo, params, {location: 'replace'});
    }
  });
}])

// custom filters
bucApp.filter('capitalize', function() {
  return function(input) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
  }
});

bucApp.filter('to_trusted', ['$sce', function($sce){
  return function(text) {
    return $sce.trustAsHtml(text);
  };
}]);
