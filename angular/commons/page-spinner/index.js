module.exports = function(app) {
  app.directive('pageSpinner', function($transitions) {
    return {
      restrict: 'E',
      replace: true,
      template: require('./page-spinner.html'),
      link: function(scope, element) {
        $transitions.onBefore({}, () => {
          element.removeClass('ng-hide');
        });
        $transitions.onFinish({}, () => {
          element.addClass('ng-hide');
        });
        $transitions.onError({}, () => {
          element.addClass('ng-hide');
        });
        /* $rootScope.$on('$stateChangeStart', function(event, currentRoute, previousRoute) {
          console.log('detected change route start', event, currentRoute, previousRoute);
          if (previousRoute) return;
          $timeout(function() {
            element.removeClass('ng-hide');
          });
        });
        $rootScope.$on('$stateChangeSuccess', function() {
          element.addClass('ng-hide');
        }); */
      }
    };
  });
}
