module.exports = function(app) {
  app.directive('footer', function() {
    return {
      restrict: 'A',
      scope: {},
      replace: true,
      template: require('./footer.html')
    }
  });
};
