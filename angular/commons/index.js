module.exports = function(app) {
  require('./helpers')(app);
  require('./page-spinner')(app);
  require('./header')(app);
  require('./footer')(app);
}
