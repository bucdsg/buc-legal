module.exports = function(app) {
  app.directive('newActivity', function($uibModal) {
    return {
      restrict: 'E',
      replace: true,
      template: require('./new-activity.html'),
      controller: function($scope, $uibModal) {
        console.log('controller here', $scope)
        $scope.showActivityModal = function() {
          console.log('showing modal', $uibModal)

          var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            template: require('./new-activity-modal.html'),
            controller: 'newActivityModal',
            size: 'md',
            resolve: {
              activityObject: function () {
                return {
                  object: 'client',
                  id: 1
                }
              }
            }
          })

          modalInstance.result.then(function (val) {
            console.log('modal ok, back to main page', val)
          }, function () {
            console.log('modal dismissed')
          })
        }
      },
      link: function(scope, element, attrs, ctrl) {
        console.log('link')
        console.log('scope', scope)
        console.log('element', element)
        console.log('attrs', attrs)
        console.log('ctrl', ctrl)
      }
    }
  })

  require('./new-activity-modal-controller')(app)
}
