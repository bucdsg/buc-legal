module.exports = function(app) {
  app.directive('header', function() {
    return {
      restrict: 'A',
      scope: {},
      replace: true,
      template: require('./header.html')
    }
  });
  app.directive('topNav', function() {
    return {
      restrict: 'E',
      scope: '&',
      replace: true,
      template: require('./top-nav.html')
    }
  });
  app.directive('menuBar', function() {
    return {
      restrict: 'E',
      scope: '&',
      replace: true,
      template: require('./menu-bar.html'),
      controller: function($scope, $state) {
        $scope.links = [
          {
            title: 'Dashboard',
            routeName: 'dashboard',
            iClass: 'site-menu-icon wb-grid-4',
            active: false
          },
          {
            title: 'Projects',
            routeName: 'project',
            iClass: 'site-menu-icon fa fa-building-o',
            active: false,
            subMenu: [
              {title: 'New Project', routeName: 'project.new'}
            ]
          },
          {
            title: 'Clients',
            routeName: 'client',
            iClass: 'site-menu-icon fa fa-users',
            active: false
          },
          {
            title: 'Payments',
            routeName: 'payments',
            iClass: 'site-menu-icon fa fa-money',
            active: false
          },
          {
            title: 'Settings',
            routeName: 'settings',
            iClass: 'site-menu-icon icon wb-settings',
            active: false
          }
        ];
      }
    }
  });
};
