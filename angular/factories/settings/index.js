module.exports = function(app) {
  app.factory('SettingsFactory', function (HelperFactory) {
    return {
      getUserSettings: () => HelperFactory.promiseRequest('/api/get-user-settings'),
      saveUserSettings: (data) => HelperFactory.promiseRequest('/api/save-user-settings', 'POST', data),
      getDocumentTemplates: () => HelperFactory.promiseRequest('/api/document-template'),
      setDocumentTemplates: (data) => HelperFactory.promiseRequest('/api/document-template', 'POST', data)
    };
  });
}
