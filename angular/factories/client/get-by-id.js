module.exports = (app, helper) => {
  return (id) => {
    return helper.promiseRequest('/api/client/' + id, 'GET');
  };
};
