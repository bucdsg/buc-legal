module.exports = (app, helper) => {
  return (data) => {
    return helper.promiseRequest('/api/client/' + data.id, 'PUT', data);
  };
};
