module.exports = function(app) {
  app.factory('ClientFactory', function (HelperFactory) {
    return {
      getAll: require('./get-all')(app, HelperFactory),
      getById: require('./get-by-id')(app, HelperFactory),
      updateClient: require('./update-client')(app, HelperFactory)
    };
  });
}
