module.exports = (app, helper) => {
  const getAll = function() {
    return helper.promiseRequest('/api/client', 'GET');
  };

  return getAll;
};
