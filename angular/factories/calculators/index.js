module.exports = function(app) {
  app.factory('CalculatorsFactory', function (HelperFactory) {
    return {
      stateOfCompletion: (data) => HelperFactory.promiseRequest('/api/calculators/stage-completion', 'POST', data)
    };
  });
}
