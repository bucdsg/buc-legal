module.exports = function(app) {
  app.factory('MilestoneFactory', function (HelperFactory) {
    return {
      stageCompletionTemplate: require('./milestone-template')(),
      getStageCompletionTemplateById: require('./get-stage-completion-template-by-id')(),
      getByTemplateId: require('./get-by-template-id')(app, HelperFactory),
      setUnitStageCompletionDate: require('./set-unit-stage-completion-date')(app, HelperFactory),
      sendNotice: require('./send-notice')(app, HelperFactory),
      resendNotice: require('./resend-notice')(app, HelperFactory),
      markResolve: require('./mark-resolve')(app, HelperFactory),
      getUnpaid: () => HelperFactory.promiseRequest('api/get-unpaid-stages'),
      setPaymentDate: (completionId, paymentDate) => HelperFactory.promiseRequest('api/set-stage-payment-date', 'POST', {completionId, paymentDate})
    };
  });
}
