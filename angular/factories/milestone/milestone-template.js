module.exports = () => [
  {
    id: 100,
    name: 'upon the grant of option to purchase',
    percentage: 0.05,
    status: 'pending'
  },
  {
    id: 101,
    name: 'upon signing S & P',
    percentage: 0.15,
    status: 'pending'
  },
  {
    id: 1,
    name: 'completion of foundation work',
    percentage: 0.10,
    status: 'pending'
  },
  {
    id: 2,
    name: 'completion of reinforced concrete framework of unit',
    percentage: 0.10,
    status: 'pending'
  },
  {
    id: 3,
    name: 'completion of brick walls of unit',
    percentage: 0.05,
    status: 'pending'
  },
  {
    id: 4,
    name: ' completion of roofing/ceiling of unit',
    percentage: 0.05,
    status: 'pending'
  },
  {
    id: 5,
    name: 'completion of electrical wiring, internal plastering, plumbing and instalation of door and window frames of unit',
    percentage: 0.05,
    status: 'pending'
  },
  {
    id: 6,
    name: 'completion of car park, roads and drains serving the housing project',
    percentage: 0.05,
    status: 'pending'
  },
  {
    id: 7,
    name: 'note of vacant possession',
    percentage: 0.25,
    status: 'pending'
  },
  {
    id: 8,
    name: ' on completion date',
    percentage: 0.15
  }
];
