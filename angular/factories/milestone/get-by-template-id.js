module.exports = (app, helper) => {
  return (id) => {
    return helper.promiseRequest('/api/milestone/template' + id, 'GET');
  };
};
