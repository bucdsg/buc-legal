module.exports = (app, helper) => {
    return (id) => {
      return helper.promiseRequest('api/notice/' + id + '/send', 'GET');
    };
  };
  