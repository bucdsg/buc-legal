const template = require('./milestone-template')()

module.exports = () => {
  return (stageId) => template.find((stage) => stage.id == stageId);
};
