module.exports = (app, helper) => {
    return (data) => {
      return helper.promiseRequest('/api/set-completion-date', 'POST', data);
    };
  };
  