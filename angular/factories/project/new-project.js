module.exports = (app, helper) => {
  return (data) => {
    return helper.promiseRequest('/api/project/new/init', 'POST', data);
  };
};
