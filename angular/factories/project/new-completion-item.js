module.exports = (app, helper) => {
  return (id, data) => {
    return helper.promiseRequest('/api/project/' + id + '/completion-items/new', 'POST', data);
  };
};
