module.exports = (app, helper) => {
  const getAll = function() {
    return helper.promiseRequest('/api/project', 'GET');
  };

  return getAll;
};
