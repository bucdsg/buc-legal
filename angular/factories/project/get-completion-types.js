module.exports = (app, helper) => {
  return (id) => {
    return helper.promiseRequest('/api/completion-items', 'GET');
  };
};
