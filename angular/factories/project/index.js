module.exports = function(app) {
  app.factory('ProjectFactory', function (HelperFactory) {
    return {
      getAll: require('./get-all')(app, HelperFactory),
      getById: require('./get-by-id')(app, HelperFactory),
      newProject: require('./new-project')(app, HelperFactory),
      updateProjectDetails: (id, data) => HelperFactory.promiseRequest(`/api/project/${id}`, 'POST', data),
      getCompletionTypes: require('./get-completion-types')(app, HelperFactory),
      newCompletionItem: require('./new-completion-item')(app, HelperFactory),
      setCompletionItems: (data) => HelperFactory.promiseRequest('/api/set-stage-completions', 'POST', data),
      propertiesAndCompletions: (id) => HelperFactory.promiseRequest('/api/project/' + id + '/properties/stage-completions')
    };
  });
}
