module.exports = (app, helper) => {
  return (id) => {
    return helper.promiseRequest('/api/project/' + id, 'GET');
  };
};
