module.exports = function(app) {
  app.factory('StatsFactory', function (HelperFactory) {
    return {
      dashboardStats: require('./dashboard-stats')(app, HelperFactory),
      propertyStageSummary: (projectId) => HelperFactory.promiseRequest(`/api/dashboard/stage-summary-by-project/${projectId}`)
    };
  });
}
