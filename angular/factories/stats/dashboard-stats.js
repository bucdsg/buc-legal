module.exports = (app, helper) => {
  return () => {
    return helper.promiseRequest('/api/dashboard/stats');
  };
};
