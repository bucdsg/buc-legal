module.exports = function(app) {
  app.factory('HelperFactory', function ($q, $http) {
    return {
      isResponseSuccess: function (response) {
        var payload = response.data;
        if ((response.status >= 200 ||  response.status < 300) && payload.status == 'success')
          return true;

        return false;
      },
      getPayloadData: function (response) {
        var payload = response.data;
        return payload.data;
      },
      promiseRequest: function(_url, _method, _data) {
        var _this = this;
        return $q(function(resolve, reject) {
          var httpOptions = {
            url: window.__baseUrl + _url.replace(/^\/|\/$/g, ''),
            method: _method || 'GET'
          }

          if (_data) {
            httpOptions.data = _data;
          }

          $http(httpOptions).then(
            function success (response) {
              if (_this.isResponseSuccess(response)) {
                resolve(_this.getPayloadData(response));
              } else {
                reject(response.data.message);
              }
            },
            function fail (error) {
              reject('Fail to load api');
            }
          );
        });
      }
    };
  });
}
