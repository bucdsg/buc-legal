module.exports = function(app) {
  app.factory('ActivityFactory', function (HelperFactory) {
    return {
      saveActivity: (data) => HelperFactory.promiseRequest('/api/save-activity', 'POST', data),
      getActivities: (data) => HelperFactory.promiseRequest('/api/get-activities', 'POST', data)
    };
  });
}
