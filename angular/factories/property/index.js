module.exports = function(app) {
  app.factory('PropertyFactory', function (HelperFactory) {
    return {
      getById: (id) => {
        return HelperFactory.promiseRequest('/api/property/' + id, 'GET');
      },
      assignToNewClient: (propertyId, data) => {
        return HelperFactory.promiseRequest('/api/property/new/client/' + propertyId, 'POST', data);
      }
    };
  });
}
