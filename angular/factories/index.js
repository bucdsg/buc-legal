module.exports = function(app) {
  require('./helpers/request-helper')(app);

  require('./activity')(app);
  require('./calculators')(app);
  require('./project')(app);
  require('./milestone')(app);
  require('./property')(app);
  require('./client')(app);
  require('./stats')(app);
  require('./settings')(app);
};
